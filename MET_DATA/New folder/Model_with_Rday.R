#- load the model
library(plantecophys)

#load("AciTfits,RData")
source("Tresponse_Vcmax_Jmax.R")

# define the temperatures to model, assume a constant dewpoint (so VPD increases with temperature)
Temps <- seq(15,45,by=0.1)
Tdew=15
VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)

#model photosynthesis. allow Ci to change

#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#WTC2: E. globulus

#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------


#-parameters: Winter (Ambient temperature)
#data<-subset(wtc2.a,wtc2.a$Temp.treat=="ambient" & wtc2.a$Season=="August")

vcmax<-eglob.vcmax[[1]][[1]]
eav<-eglob.vcmax[[2]][[1]]*1000
delsv<-eglob.vcmax[[3]][[1]]*1000

jmax<-eglob.jmax[[1]][[1]]
eaj<-eglob.jmax[[2]][[1]]*1000
delsj<-eglob.jmax[[3]][[1]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-0.66875849
Q10<-2

#with default Rday settings

#win.cold <- Photosyn(Tleaf=data$Tleaf,VPD=data$VpdL,Ci=data$Ci,
                     #Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     #Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-0.66875849+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


windows(100,60);par(mfrow=c(1,2))


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. globulus, Winter/Ambient T")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

#points(Photo~Tleaf,data=subset(wtc2.a,wtc2.a$Temp.treat=="ambient" & wtc2.a$Season=="August"),pch=16,col="red")

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)

#------------------------------------------------------------------------------------------------------------------------

#-parameters: Winter (Elevated temperature)

vcmax<-eglob.vcmax[[1]][[6]]
eav<-eglob.vcmax[[2]][[6]]*1000
delsv<-eglob.vcmax[[3]][[6]]*1000

jmax<-eglob.jmax[[1]][[2]]
eaj<-eglob.jmax[[2]][[2]]*1000
delsj<-eglob.jmax[[3]][[2]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.19597034
Q10<-2

#with default Rday settings
win.warm <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.warm.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.warm.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)


#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-1.19597034+0*Temps

win.warm.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)




plot(ALEAF~Tleaf,dat=win.warm,type="l",lwd=2,col="black",ylim=c(5,35),main="E. globulus, Winter/Elevated T")
lines(ALEAF~Tleaf,dat=win.warm.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.warm.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.warm.3,type="l",lwd=2,col="pink",lty=2)

#points(Photo~Tleaf,data=subset(wtc2.a,wtc2.a$Temp.treat=="elevated" & wtc2.a$Season=="August"))

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)

#----------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------------

#-parameters: Summer (Ambient temperature)

vcmax<-eglob.vcmax[[1]][[2]]
eav<-eglob.vcmax[[2]][[2]]*1000
delsv<-eglob.vcmax[[3]][[2]]*1000

jmax<-eglob.jmax[[1]][[3]]
eaj<-eglob.jmax[[2]][[3]]*1000
delsj<-eglob.jmax[[3]][[3]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-0.60159262
Q10<-2

#with default Rday settings
sum.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
sum.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
sum.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)


#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-0.60159262+0*Temps

sum.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)



windows(100,60);par(mfrow=c(1,2))

#- Ambient
plot(ALEAF~Tleaf,dat=sum.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. globulus, Summer/Ambient T")
lines(ALEAF~Tleaf,dat=sum.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=sum.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=sum.cold.3,type="l",lwd=2,col="pink",lty=2)

#points(Photo~Tleaf,data=subset(wtc2.a,wtc2.a$Temp.treat=="ambient" & wtc2.a$Season=="December"))

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)

#-parameters: Summer (Elevated temperature)

vcmax<-eglob.vcmax[[1]][[4]]
eav<-eglob.vcmax[[2]][[4]]*1000
delsv<-eglob.vcmax[[3]][[4]]*1000

jmax<-eglob.jmax[[1]][[5]]
eaj<-eglob.jmax[[2]][[5]]*1000
delsj<-eglob.jmax[[3]][[5]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-0.58380429
Q10<-2

#with default Rday settings
sum.warm <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
sum.warm.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
sum.warm.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-0.58380429+0*Temps

sum.warm.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)



plot(ALEAF~Tleaf,dat=sum.warm,type="l",lwd=2,col="black",ylim=c(5,35),main="E. globulus, Summer/Elevated T")
lines(ALEAF~Tleaf,dat=sum.warm.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=sum.warm.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=sum.warm.3,type="l",lwd=2,col="pink",lty=2)

#points(Photo~Tleaf,data=subset(wtc2.a,wtc2.a$Temp.treat=="ambient" & wtc2.a$Season=="December"))

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)



#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#WTC3: E. tereticornis

#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#-parameters: Winter (Ambient temperature)

vcmax<-etret.vcmax[[1]][[3]]
eav<-etret.vcmax[[2]][[3]]*1000
delsv<-etret.vcmax[[3]][[3]]*1000

jmax<-etret.Jmax[[1]][[4]]
eaj<-etret.Jmax[[2]][[4]]*1000
delsj<-etret.Jmax[[3]][[4]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.6596714
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-1.6596714+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


windows(100,60);par(mfrow=c(1,2))


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tereticornis, Winter/Ambient T")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)



legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)


#---------------------------------------

#-parameters: Winter (Elevated temperature)

vcmax<-etret.vcmax[[1]][[6]]
eav<-etret.vcmax[[2]][[6]]*1000
delsv<-etret.vcmax[[3]][[6]]*1000

jmax<-etret.Jmax[[1]][[6]]
eaj<-etret.Jmax[[2]][[6]]*1000
delsj<-etret.Jmax[[3]][[6]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.0340337
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-1.0340337+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tereticornis, Winter/Elevated T")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)


#-------------------------------------------------

#-parameters: Summer (Ambient temperature)

vcmax<-etret.vcmax[[1]][[2]]
eav<-etret.vcmax[[2]][[2]]*1000
delsv<-etret.vcmax[[3]][[2]]*1000

jmax<-etret.Jmax[[1]][[3]]
eaj<-etret.Jmax[[2]][[3]]*1000
delsj<-etret.Jmax[[3]][[3]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.4304719 #rate at 25C
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-1.4304719+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


windows(100,60);par(mfrow=c(1,2))


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tereticornis, Summer/Ambient T")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)


#---------------------------------------

#-parameters: Summer (Elevated temperature)

vcmax<-etret.vcmax[[1]][[5]]
eav<-etret.vcmax[[2]][[5]]*1000
delsv<-etret.vcmax[[3]][[5]]*1000

jmax<-etret.Jmax[[1]][[5]]
eaj<-etret.Jmax[[2]][[5]]*1000
delsj<-etret.Jmax[[3]][[5]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.0340337
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-1.0340337+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tereticornis, Summer/Elevated T")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)

#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#WTC4: E. parramattensis

#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#-parameters: Spring (Ambient temperature)

vcmax<-eparamat.Vcmax[[1]][[1]]
eav<-eparamat.Vcmax[[2]][[1]]*1000
delsv<-eparamat.Vcmax[[3]][[1]]*1000

jmax<-eparamat.Jmax[[1]][[1]]
eaj<-eparamat.Jmax[[2]][[1]]*1000
delsj<-eparamat.Jmax[[3]][[1]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-0.9669966
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-0.9669966+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


windows(100,60);par(mfrow=c(1,2))


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. parramattensis, Spring/Ambient T")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)


#---------------------------------------

#-parameters: Spring (Elevated temperature)

vcmax<-etret.vcmax[[1]][[3]]
eav<-etret.vcmax[[2]][[3]]*1000
delsv<-etret.vcmax[[3]][[3]]*1000

jmax<-etret.Jmax[[1]][[3]]
eaj<-etret.Jmax[[2]][[3]]*1000
delsj<-etret.Jmax[[3]][[3]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.5127693
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-1.5127693+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. parramattensis, Spring/Elevated T")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)


#-------------------------------------------------

#-parameters: Winter (Ambient temperature)

vcmax<-eparamat.Vcmax[[1]][[2]]
eav<-eparamat.Vcmax[[2]][[2]]*1000
delsv<-eparamat.Vcmax[[3]][[2]]*1000

jmax<-eparamat.Jmax[[1]][[2]]
eaj<-eparamat.Jmax[[2]][[2]]*1000
delsj<-eparamat.Jmax[[3]][[2]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.6471809
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-1.6471809+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


windows(100,60);par(mfrow=c(1,2))


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. parramattensis, Winter/Ambient T")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)


#---------------------------------------

#-parameters: Winter (Elevated temperature)

vcmax<-etret.vcmax[[1]][[4]]
eav<-etret.vcmax[[2]][[4]]*1000
delsv<-etret.vcmax[[3]][[4]]*1000

jmax<-etret.Jmax[[1]][[4]]
eaj<-etret.Jmax[[2]][[4]]*1000
delsj<-etret.Jmax[[3]][[4]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.7238563
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=25)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-1.7238563+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. parramattensis, Winter/Elevated T")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)



#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#WTC4: E. tetradonta

#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#-parameters: Spring (Ambient temperature)

vcmax<-e.tr.Vcmax[[1]][[1]]
eav<-e.tr.Vcmax[[2]][[1]]*1000
delsv<-e.tr.Vcmax[[3]][[1]]*1000

jmax<-e.tr.Jcmax[[1]][[1]]
eaj<-e.tr.Jcmax[[2]][[1]]*1000
delsj<-e.tr.Jcmax[[3]][[1]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-0.5656324 #thi is value at 30C. no data at 25C
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd0=Rd0,Q10=Q10,TrefR=30)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-0.5656324+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=Rd1)


windows(60,60);par(mfrow=c(1,1))


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tetradonta")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)


#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#TMB: E. delegatensis

#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#-parameters: Summer 

vcmax<-e.del.Vcmax[[1]][[1]]
eav<-e.del.Vcmax[[2]][[1]]*1000

jmax<-e.del.Jmax[[1]][[1]]
eaj<-e.del.Jmax[[2]][[1]]*1000


#edvc=2e5
#edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-2.5658792 #thi is value at 30C. no data at 25C
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,Jmax = jmax)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,
                       Jmax = jmax,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,
                       Jmax = jmax,Rd0=Rd0,Q10=Q10,TrefR=30)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-2.5658792+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,
                       Jmax = jmax,Rd=Rd1)


windows(100,40);par(mfrow=c(1,3))


plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. delegatensis/Summer")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)


#---------------------------------------
#-parameters: Autumn

vcmax<-e.del.Vcmax[[1]][[2]]
eav<-e.del.Vcmax[[2]][[2]]*1000

jmax<-e.del.Jmax[[1]][[2]]
eaj<-e.del.Jmax[[2]][[2]]*1000


#edvc=2e5
#edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-2.4711532 #thi is value at 21C. no data at 25C
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,Jmax = jmax)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,
                       Jmax = jmax,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,
                       Jmax = jmax,Rd0=Rd0,Q10=Q10,TrefR=21)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-2.4711532+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,
                       Jmax = jmax,Rd=Rd1)




plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. delegatensis/Autumn")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)

#--------------------------------------
#-parameters: Spring

vcmax<-e.del.Vcmax[[1]][[3]]
eav<-e.del.Vcmax[[2]][[3]]*1000

jmax<-e.del.Jmax[[1]][[3]]
eaj<-e.del.Jmax[[2]][[3]]*1000


#edvc=2e5
#edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.3894537 #thi is value at 23C. no data at 25C
Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,Jmax = jmax)

#temperature response of Rday from Crous et al 2013
win.cold.1 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,
                       Jmax = jmax,Rd=Rd)


#Q10 function: Q10=2 and base rate (at 25C) fron Aci curves
win.cold.2 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,
                       Jmax = jmax,Rd0=Rd0,Q10=Q10,TrefR=23.6)

#constant Rd: magnitude equal to the base rate (at 25C) from Aci curves
Rd1<-2.4711532+0*Temps

win.cold.3 <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,
                       Jmax = jmax,Rd=Rd1)




plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. delegatensis/Spring")
lines(ALEAF~Tleaf,dat=win.cold.1,type="l",lwd=2,col="red",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.2,type="l",lwd=2,col="green",lty=2)
lines(ALEAF~Tleaf,dat=win.cold.3,type="l",lwd=2,col="pink",lty=2)

legend("top",c("Rd=Default","Rd=Crouse et al 2013","Rd=Q10=2","Rd=fixed"),lwd=2,lty=c(1,2,2,2),col=c("black","red","forestgreen","pink"),bty="n",ncol=3)


