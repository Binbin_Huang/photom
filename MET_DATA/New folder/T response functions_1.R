library("plantecophys")
library("doBy")
library("nlme")
library("dplyr")
library(readxl)
library(stringr)
library("nlstools")
library(plotBy)
library(magicaxis)
library(rgl)
library( propagate)
library(gplots)
library(ggplot2)
library(lme4)
library(lubridate)
library(car)
library(boot)

#-----------------------------------------------------------------------------------------------------------------------
#function to read and process wtcIII ACi data 

getwtc3<-function(path){
  #read wtcIII ACi data
  
  aci<-read.csv(paste(path,"/WTC_TEMP_CM_GX-ACITEMP_20130917-20140507_R_V2.csv",sep=""))
  
  
  
  #to define months
  aci$Season<-NA
  aci$Season[which(aci$MeanTemp=="14-Jan")]<-"January"
  aci$Season[which(aci$MeanTemp=="13-Sep")]<-"September"
  aci$Season[which(aci$MeanTemp=="14-Apr")]<-"April"
  aci$Season[which(aci$MeanTemp=="14-May")]<-"April"
  aci$Season<-as.factor(aci$Season)
  
  #to drop TargetTemp 35. 
  #stomatal conductance of this temperature level is very low (less than 0.09 for all 31 measurements)
  
  #aci<-aci[!aci$TargetTemp==35,]
  aci$Curve<-aci$Plotsite
  #write.csv(aci,"C:/Dushan/Repos//ACI_ALL/WTC3/WTC3_ACidata_processed.csv",row.names=FALSE,sep=",")
  
  return(aci)  
}


#------------------------------------------------------------------------------------------------------------------------

#function to read and process wtcII ACi data 

getwtc2<-function(path){
  #read wtcIII ACi data
  
  aciG<-read.csv(paste(path,"/WTC_Temp&CO2_CM_GX-AciTemp_20101202-20110908_R.csv",sep=""))
  
  #to define temperatures
  aciG$TargetTemp<-NA
  aciG$TargetTemp[which(aciG$Species=="Eglob @ 25C")]<-25
  aciG$TargetTemp[which(aciG$Species=="Eglob @ 20C")]<-20
  aciG$TargetTemp[which(aciG$Species=="Eglob @ 30C")]<-30
  aciG$TargetTemp[which(aciG$Species=="Eglob @ 35C")]<-35
  aciG$TargetTemp[which(aciG$Species=="Eglob @ 41C")]<-41
  aciG$TargetTemp[which(aciG$Species=="Eglob @ 17C")]<-17
  aciG$TargetTemp[which(aciG$Species=="Eglob")]<-25
  aciG$TargetTemp<-as.numeric(aciG$TargetTemp)
  
  #to extract month
  aciG$Date<- as.Date(mdy(aciG$Date))
  aciG$Month<-month(aciG$Date)
  
  
  
  
  aciG$Season<-ifelse(aciG$Month==02,"February",
                      ifelse(aciG$Month==08,"August",
                             ifelse(aciG$Month==09,"August",
                                    ifelse(aciG$Month==12,"December",
                                           ifelse(aciG$Month==04,"April",NA)))))
  
  aciG$Season<-as.factor(aciG$Season) 
  aciG$Curve<-aciG$Identity
  
  #write.table(aciG,"C:/Dushan/Repos/ACI_ALL/WTC2/WTC2_ACidata_processednew.csv",row.names=FALSE,sep=",")
  return(aciG)  
  

  
  
}


#------------------------------------------------------------------------------------------------------------------------
#function to read and process Tumbarumba ACi data 

getTb<-function(path="C:/Dushan/R/WTCIII"){
  
  #read  ACi data
  
  aciT<-read.csv(paste(path,"/rawdata/TumbarumbaGasex_ACis_Medlyn.csv",sep=""))
  
  #aciT$M<-month(as.POSIXlt(aciT$Date, format="%d/%m/%Y"))
  
  aciT$Date<- as.Date(dmy(aciT$Date))
  t<-data.frame(
    DateTime=aciT$Date,
    Season=format(as.POSIXct(aciT$Date, format="%d-%m-%y"), format="%m"))
  t$index<-c(1:702)
  aciT$index<-c(1:702)
  aciT<-merge(aciT,t,by=c("index"))
  
  aciT<-subset(aciT,aciT$PARi>1200)
  
  aciT$Species<-"E.delegatensis"

  aciT$Month<-NA
  aciT$Month[which(aciT$Season=="02")]<-"February"
  aciT$Month[which(aciT$Season=="05")]<-"May"
  aciT$Month[which(aciT$Season=="11")]<-"November"
  
  
  write.csv(aciT,"C:/Dushan/Repos//ACI_ALL/TMB/Tumbarumba_ACidata_processed.csv",row.names=FALSE)
  return(aciT)  
  
}
#getTb()
#-----------------------------------------------------------------------------------------------------------------------

#function to read and process WTCIII DIURNAL Photosynthesis data 
getD<-function(path="C:/Dushan/R/WTCIII"){
  
  #read  ACi data
  
  dataD<-read.csv(paste(path,"/rawdata/WTC_TEMP_CM_GX-DIURNAL_20130710-20140220_L1_v2.csv",sep=""))
  
  #Allocation of curve no for each canopy position in each campaign
  
  dataD$Number <- c(1)
  Number <- c()
  count <- 1  
  for (i in 2:length(dataD$campaign)){
    ifelse(dataD$position[i-1] == dataD$position[i],count <- count,count <- count + 1)
    Number[i] <- count 
  }
  dataD$Number[2:length(dataD$position)] <- na.omit(Number)
  
  return(dataD)
  
}

#-----------------------------------------------------------------------------------------------------------------------
#function to read and process HFE Commongarden ACi data (Lin's data) 

gethfe<-function(path){
  
  #read HFE Commongarden ACi data 
  
  acihfe<-read.csv(paste(path,"/HFE_CommonGarden_ACI_3Seasons_YSLin2012.csv",sep=""))
  
  
  
  #to define months
  acihfe$Month<-NA
  acihfe$Month[which(acihfe$season==1)]<-"August"
  acihfe$Month[which(acihfe$season==2)]<-"November"
  acihfe$Month[which(acihfe$season==3)]<-"February"
  
  acihfe$season<-as.factor(acihfe$season)
  
  return(acihfe)  
}



#-----------------------------------------------------------------------------------------------------------------------


# fit temperature response of Vcmax and get CI of parameters


fVc <- as.formula(Vcmax ~ k25 * exp((Ea*(TsK - 298.15))/(298.15*0.008314*TsK)) * 
                    (1+exp((298.15*delS - 200)/(298.15*0.008314))) / 
                    (1+exp((TsK*delS-200)/(TsK*0.008314))))

fVJ <- as.formula(Jmax ~ k25 * exp((Ea*(TsK - 298.15))/(298.15*0.008314*TsK)) * 
                    (1+exp((298.15*delS - 200)/(298.15*0.008314))) / 
                    (1+exp((TsK*delS-200)/(TsK*0.008314))))


fVc.a<-as.formula(Vcmax ~ k25 * exp((Ea*(TsK - 298.15))/(298.15*0.008314*TsK)))
fVJ.a<-as.formula(Jmax ~ k25 * exp((Ea*(TsK - 298.15))/(298.15*0.008314*TsK)))

# peaked model (estimation of predictions and 95% CI dissabled to reduce impliment time)

fitpeaked<-function(dat){
  
  try(Vc<-nls(fVc, start = list(k25=100, Ea=60, delS = 0.64), data = dat))
  Vc2<-summary(Vc)
  res<-Vc2$coefficients[1:6]
  names(res)[1:6]<-c("Vcmax25","EaV","delsV","Vcmax25.se","EaV.se","delsV.se")
  
  #tt<-seq(min(dat$TsK),max(dat$TsK),length=51)
  #predicts<-predictNLS(Vc,newdata=data.frame(TsK=tt),interval="confidence",level=0.95)
 # predicts.df<-data.frame(predicts$summary)
  #predicts.df$TsK<-tt
  
  
  #return(list(results,predicts.df))
  r<-cor(fitted(Vc),dat$Vcmax)
  r2<-r*r
  
  #test for normality of residuals
  rest<-residuals(Vc)
  norm<-shapiro.test(rest)
  s<-norm$statistic
  pvalue<-norm$p.value
  
  param<-c(res,r2,s,pvalue)
  names(param)[1:9]<-c("Vcmax25","EaV","delsV","Vcmax25.se","EaV.se","delsV.se","r2","s","pvalue")
  return(param)
  
#return(c(res,r2,s,pvalue))
  
  
  #return(list(res,predicts.df))
  #return(list(res))
}

# fit temperature response of Jmax and get CI of parameters
# peaked model (estimation of predictions and 95% CI dissabled to reduce impliment time)

fitpeakedJ<-function(dat){
  
  try(Vj<-nls(fVJ, start = list(k25=100, Ea=70, delS = 0.63), data = dat))
  Vj2<-summary(Vj)
  res<-Vj2$coefficients[1:6]
  names(res)[1:6]<-c("Jmax25","EaJ","delsJ","Jmax25.se","EaJ.se","delsJ.se")
  
  #tt<-seq(min(dat$TsK),max(dat$TsK),length=51)
  #predicts<-predictNLS(Vj,newdata=data.frame(TsK=tt),interval="confidence",level=0.95)
  #predicts.df<-data.frame(predicts$summary)
  #predicts.df$TsK<-tt
  
  #return(list(results,predicts.df))
  r<-cor(fitted(Vj),dat$Jmax)
  r2<-r*r
  
  #test for normality of residuals
  rest<-residuals(Vj)
  norm<-shapiro.test(rest)
  s<-norm$statistic
  pvalue<-norm$p.value
  
  param<-c(res,r2,s,pvalue)
  names(param)[1:9]<-c("Vcmax25","EaV","delsV","Vcmax25.se","EaV.se","delsV.se","r2","s","pvalue")
  return(param)
  
  #return(c(res,r2,s,pvalue))
  #return(list(res))
}

# function to fit temperature response of Vcmax and get CI for parameters
# standard Arrhenius model

fitarh<-function(dat){
  
  try(Vr<-nls(fVc.a, start = list(k25=100, Ea=40), data = dat))
  Vr2<-summary(Vr)
  res<-Vr2$coefficients[1:4]
  names(res)[1:4]<-c("Vcmax25","EaV","Vcmax25.se","EaV.se")
  
  #tt<-seq(min(dat$TsK),max(dat$TsK),length=51)
  #predicts<-predictNLS(Vr,newdata=data.frame(TsK=tt),interval="confidence",level=0.95)
  #predicts.df<-data.frame(predicts$summary)
  #predicts.df$TsK<-tt
  #return(list(results,predicts.df))
  
  r<-cor(fitted(Vr),dat$Vcmax)
  r2<-r*r
  
  #test for normality of residuals
  rest<-residuals(Vr)
  norm<-shapiro.test(rest)
  s<-norm$statistic
  pvalue<-norm$p.value
  
  
  return(c(res,r2,s,pvalue))
  #return(list(res,predicts.df))
  #return(res)
}


#for Jmax

fitarhJ<-function(dat){
  
  try(Vr<-nls(fVJ.a, start = list(k25=100, Ea=40), data = dat))
  Vr2<-summary(Vr)
  res<-Vr2$coefficients[1:4]
  names(res)[1:4]<-c("Jmax25","EaJ","Jmax25.se","EaJ.se")
  
  #tt<-seq(min(dat$TsK),max(dat$TsK),length=51)
  #predicts<-predictNLS(Vr,newdata=data.frame(TsK=tt),interval="confidence",level=0.95)
  #predicts.df<-data.frame(predicts$summary)
  #predicts.df$TsK<-tt
  
  r<-cor(fitted(Vr),dat$Jmax)
  r2<-r*r
  
  #test for normality of residuals
  rest<-residuals(Vr)
  norm<-shapiro.test(rest)
  s<-norm$statistic
  pvalue<-norm$p.value
  
  
  return(c(res,s,pvalue))
  
  
  #return(res)
  #return(list(res,predicts.df))
}



#----------------------------------------------------------------------------------------------------------------------
# from Jhon's codes
#- function to fit the June et al. (2004) FPB model for the temperature response of photosynthesis.
#- accepts a dataframe, returns a list with [1] named vector of parameter estiamtes and their se's,
#-   and [2] a dataframe with the predictions and 95% confidence intervals.
fitAvT <- function(dat){
  try(A_Topt <- nls(Photo~ Jref*exp(-1*((Tleaf-Topt)/theta)^2),data=dat,start=list(Jref=20,Topt=25,theta=20)))
  A_Topt2 <- summary(A_Topt)
  results <- A_Topt2$coefficients[1:6]
  names(results)[1:6] <- c("Aopt","Topt","theta","Aopt.se","Topt.se","theta.se")
  
  TT <- seq(min(dat$Tleaf),max(dat$Tleaf),length=50)
  predicts <- predictNLS(A_Topt, newdata=data.frame(Tleaf = TT),interval="confidence",level=0.95)
  predicts.df <- data.frame(predicts$summary)
  predicts.df$Tleaf <- TT
  
  return(list(results,predicts.df))
}



#-----------------------------------------------------------------------------------------
#function to fit the Quadratic model for the temperature response of photosynthesis.
#model, Anet=Aopt-b(Tleaf-Topt)^2
#- accepts a dataframe, returns a list with [1] named vector of parameter estiamtes and their se's,
#-   and [2] a dataframe with the predictions and 95% confidence intervals.

fitquad<-function(dat){
  try(An<-nls(Photo~Aopt-(b*(Tleaf-Topt)^2),data=dat,start=list(Aopt=max(dat$Photo),Topt=25,b=0.05)))
  A.1<-summary(An)
  results<-(A.1$coefficients[1:6])
  names(results)[1:6] <- c("aopt","topt","b","aopt.se","topt.se","b.se")
  
  #TT.i <- seq(min(dat$Tleaf),max(dat$Tleaf),length=51)
  #predicts <- predictNLS(An, newdata=data.frame(Tleaf = TT.i),interval="confidence",level=0.95)
  #predicts.df <- data.frame(predicts$summary)
  #predicts.df$Tleaf <- TT.i
  
  r<-cor(fitted(An),dat$Photo)
  r2<-r*r
  
  #test for normality of residuals
  rest<-residuals(An)
  norm<-shapiro.test(rest)
  s<-norm$statistic
  pvalue<-norm$p.value
  
  return(c(results,r2,s,pvalue))
  
}

#------------------------------------------------------------------------------------------
#function to fit the Quadratic model for the temperature response of photosynthesis at CI=300.
#model, Anet=Aopt-b(Tleaf-Topt)^2
#- accepts a dataframe, returns a list with [1] named vector of parameter estiamtes and their se's,
#-   and [2] a dataframe with the predictions and 95% confidence intervals.

fitquad300<-function(dat){
  
  try(An<-nls(lowAs~Aopt-(b*(Ts-Topt)^2),data=dat,start=list(Aopt=max(dat$lowAs),Topt=25,b=0.05)))
  
  
  A.1<-summary(An)
  results<-(A.1$coefficients[1:6])
  names(results)[1:6] <- c("Aopt","Topt","b","Aopt.se","Topt.se","b.se")
  
  #TT.i <- seq(min(dat$Ts),max(dat$Ts),length=51)
  #predicts <- predictNLS(An, newdata=data.frame(Ts = TT.i),interval="confidence",level=0.95)
  #predicts.df <- data.frame(predicts$summary)
  #predicts.df$Ts <- TT.i
  
  #return(list(results,predicts.df))
  r<-cor(fitted(An),dat$Photo)
  r2<-r*r
  
  #test for normality of residuals
  rest<-residuals(An)
  norm<-shapiro.test(rest)
  s<-norm$statistic
  pvalue<-norm$p.value
  

  return(c(results,r2,s,pvalue,ci))
  
  
  
  
  #return(results)
}

#------------------------------------------------------------------------------------------------------------------------
#function to fit the Quadratic model for the temperature response of photosynthesis at CI=800.
#model, Anet=Aopt-b(Tleaf-Topt)^2
#- accepts a dataframe, returns a list with [1] named vector of parameter estiamtes and their se's,
#-   and [2] a dataframe with the predictions and 95% confidence intervals.


fitquad800<-function(dat){
  
  try(An<-nls(highAs~Aopt-(b*(Ts-Topt)^2),data=dat,start=list(Aopt=max(dat$highAs),Topt=25,b=0.05)))
  
  
  A.1<-summary(An)
  results<-(A.1$coefficients[1:6])
  names(results)[1:6] <- c("Aopt","Topt","b","Aopt.se","Topt.se","b.se")
  
  #TT.i <- seq(min(dat$Ts),max(dat$Ts),length=51)
  #predicts <- predictNLS(An, newdata=data.frame(Ts = TT.i),interval="confidence",level=0.95)
  #predicts.df <- data.frame(predicts$summary)
  #predicts.df$Ts <- TT.i
  
  #return(list(results,predicts.df))
  
  #return(list(results,predicts.df))
  r<-cor(fitted(An),dat$Photo)
  r2<-r*r
  
  #test for normality of residuals
  rest<-residuals(An)
  norm<-shapiro.test(rest)
  s<-norm$statistic
  pvalue<-norm$p.value
  
  #return(c(results,r2,s,pvalue))
  
  
  
  
  
  return(results)
}


#---------------------------------------------------------------------------------------------------------------------

#- function to get the confidence intervals of an NLS fit
#https://quantitativeconservationbiology.wordpress.com/2013/07/02/confidence-interval-for-a-model-fitted-with-nls-in-r/

as.lm.nls <- function(object, ...) {
  if (!inherits(object, "nls")) {
    w <- paste("expected object of class nls but got object of class:", 
               paste(class(object), collapse = " "))
    warning(w)
  }
  
  gradient <- object$m$gradient()
  if (is.null(colnames(gradient))) {
    colnames(gradient) <- names(object$m$getPars())
  }
  
  response.name <- if (length(formula(object)) == 2) "0" else 
    as.character(formula(object)[[2]])
  
  lhs <- object$m$lhs()
  L <- data.frame(lhs, gradient)
  names(L)[1] <- response.name
  
  fo <- sprintf("%s ~ %s - 1", response.name, 
                paste(colnames(gradient), collapse = "+"))
  fo <- as.formula(fo, env = as.proto.list(L))
  
  do.call("lm", list(fo, offset = substitute(fitted(object))))
  
}

#----------------------------------------------------------------------------------------------------------------
# Adds error bars to a plot
# From John's codes

adderrorbars <- function(x,y,SE,direction,barlen=0.04,...){
  
  if(length(direction)>1)stop("direction must be of length one.")
  #if(direction == "updown")
  #  direction <- c("up","down")
  if(direction == "rightleft" | direction == "leftright")direction <- c("left","right")
  
  if("up" %in% direction)
    arrows(x0=x, x1=x, y0=y, y1=y+SE, code=3, angle=90, length=barlen,...)
  if("down" %in% direction) 
    arrows(x0=x, x1=x, y0=y, y1=y-SE, code=3, angle=90, length=barlen,...)
  if("updown" %in% direction) 
    arrows(x0=x, x1=x, y0=y+SE, y1=y-SE, code=3, angle=90, length=barlen,...)
  if("left" %in% direction) 
    arrows(x0=x, x1=x-SE, y0=y, y1=y, code=3, angle=90, length=barlen,...)
  if("right" %in% direction)
    arrows(x0=x, x1=x+SE, y0=y, y1=y, code=3, angle=90, length=barlen,...)  
  
}
#----------------------------------------------------------------------------------------------------------------


#is.even <- function(x){ x %% 2 == 0 } 
#aci$c<-ifelse(is.even(aci$Chamber),"Elevated","Ambient")
#----------------------------------------------------------------------------------------------------------------

#function to fit BBOpti stomatal conductance model and to estimate 95% CI for g1


bbopt<-function(gsdata){
  
  fit.nls <- nls(Cond ~ 1.6 * (1 + g1/sqrt(VpdL)) * (Photo/CO2S), 
                 start = list(g1 = 4),data=gsdata)
  
  #g.boot<-nlsBoot(fit.nls,niter=50)
  g <- summary(fit.nls)
  results <- g$coefficients[1:2]
  names(results)[1:2] <- c("g1","Std.err")
  
  
  
  return(results)
}

#----------------------------------------------------------------------------------------------------------------
#function to fit BBOptiFull stomatal conductance model and to estimate 95% CI for g1

bboptful<-function(dat){
  
  fit <- nls(Cond ~ 1.6 * (1 + g1/VpdL^gk) * (Photo/CO2S), 
             start = list(g1 = 4, gk = 0.5),data=dat)
  
  fit2 <- summary(fit)
  ci_g1<-confint2(fit)
  results <- fit2$coefficients[1:4]
  names(results)[1:4] <- c("g1","gk","g1.se","gk.se")
  
  
  #g.boot<-nlsBoot(fit,niter=999)
  
  #vpd <- seq(min(dat$VpdL),max(dat$VpdL),length=50)
  #photo <- seq(min(dat$Photo),max(dat$Photo),length=50)
  #co2s <- seq(min(dat$CO2S),max(dat$CO2S),length=50)
  #predicts <- predictNLS(fit, newdata=data.frame(VpdL = vpd,Photo=photo,CO2S=co2s),interval="confidence",level=0.95)
  #predicts.df <- data.frame(predicts$summary)
  
  
  return(list(results,ci_g1))
}

#------------------------------------------------------------------------------------------------------------------
#function to fit quadratic form to Photosynthetic temperature response
#using dataframe from summaryBy function
#quadratic
fitquadnew<-function(dat){
  try(An<-nls(Photo.mean~Aopt-(b*(Tleaf.mean-Topt)^2),data=dat,start=list(Aopt=max(dat$Photo.mean),Topt=25,b=0.05)))
  A.1<-summary(An)
  results<-(A.1$coefficients[1:6])
  names(results)[1:6] <- c("Aopt","Topt","b","Aopt.se","Topt.se","b.se")
  
  TT.i <- seq(min(dat$Tleaf.mean),max(dat$Tleaf.mean),length=51)
  predicts <- predictNLS(An, newdata=data.frame(Tleaf.mean = TT.i),interval="confidence",level=0.95)
  predicts.df <- data.frame(predicts$summary)
  predicts.df$Tleaf <- TT.i
  
  resids<-residuals(An)
  return(list(results,predicts.df,resids))
  
}
#---------------------------------------------------------------------------------------------------------------------
#June's 2004 model

fitAvTnew <- function(dat){
  try(A_Topt <- nls(Photo.mean~ Jref*exp(-1*((Tleaf.mean-Topt)/theta)^2),data=dat,start=list(Jref=20,Topt=25,theta=20)))
  A_Topt2 <- summary(A_Topt)
  results <- A_Topt2$coefficients[1:6]
  names(results)[1:6] <- c("Aopt","Topt","theta","Aopt.se","Topt.se","theta.se")
  
  TT <- seq(min(dat$Tleaf.mean),max(dat$Tleaf.mean),length=50)
  predicts <- predictNLS(A_Topt, newdata=data.frame(Tleaf.mean = TT),interval="confidence",level=0.95)
  predicts.df <- data.frame(predicts$summary)
  predicts.df$Tleaf <- TT
  
  return(list(results,predicts.df))
}

#-----------------------------------------------------------------------------------------------------------------------
#ITD curve 

fititd<-function(dat){
  try(An<-nls(Photo.mean~(b*(Tleaf.mean-Tmin)*((1-exp(c*(Tleaf.mean-Tmax))))),data=dat,start=list(b=2,Tmin=0,Tmax=50,c=0.03)))
A.1<-summary(An)
results<-(A.1$coefficients[1:6])
  #names(results)[1:6] <- c("Aopt","Topt","b","Aopt.se","Topt.se","b.se")
  
TT.i <- seq(min(dat$Tleaf.mean),max(dat$Tleaf.mean),length=51)
predicts <- predictNLS(An, newdata=data.frame(Tleaf.mean = TT.i),interval="confidence",level=0.95)
  predicts.df <- data.frame(predicts$summary)
  predicts.df$Tleaf <- TT.i
  
  return(list(results,predicts.df))
  
}

#--------------------------------------------------------------------------------------------------------------------------

#functions to fit ACi curves

getr2 <- function(x){
  lmfit <- lm(Ameas ~ Amodel, data=x$df)
  summary(lmfit)$r.squared
}

# function to calculate Topt from fitted quadratic
Topt <- function(pars) {
  -pars[2]/2/pars[3]
}

makecurves <- function(dir, fname) {
  
  data <- read.csv(paste0(dir,"/",fname) )
  
  fits1 <- fitacis(data, "Curve", fitmethod="bilinear", Tcorrect=FALSE)
  fits2 <- fitacis(data, "Curve", fitTPU=TRUE, Tcorrect=FALSE)
  
  pdf(paste0(dir,"/TPU_ornot.pdf"), width=9, height=5)
  par(mfrow=c(1,2))
  for(i in 1:length(fits1)){
    plot(fits1[[i]], main=paste(names(fits1)[i], "No TPU", sep=" - "))
    plot(fits2[[i]], main=paste(names(fits1)[i], "TPU", sep=" - "))
  }
  dev.off()
  
  return(fits2)
}


makedata<- function(dir, fname, fit) {
  
  ret <- coef(fit) 
  ret$Jmax[ret$Jmax > 1000] <- NA
  ret$rmse <- sapply(fit, "[[", "RMSE")
  ret$R2 <- sapply(fit, getr2)
  
  # extract fitted values at 275 and 800 ppm Ci
  
  #ret$Ci <- sapply(fit,function(x)min(x$df$Ci))
  ret$resp <- sapply(fit, function(x)x$Photosyn(Ci=50)$ALEAF)
  ret$lowAs <- sapply(fit, function(x)x$Photosyn(Ci=275)$ALEAF)
  ret$highAs <- sapply(fit, function(x)x$Photosyn(Ci=800)$ALEAF)
  ret$Ts <- sapply(fit, function(x)mean(x$df$Tleaf))
  ret$Condt<-sapply(fit, function(x)mean(x$df$Cond))
  ret$TsK <- ret$Ts+273.15
  ret$Tsq <- ret$Ts * ret$Ts
  ret$maxCi <- sapply(fit,function(x)max(x$df$Ci))
  ret$minCi <- sapply(fit,function(x)min(x$df$Ci))
  
  data <- read.csv(paste0(dir,"/",fname) )
  ret <- merge(ret,data,by="Curve")
  ret <- ret[!duplicated(ret[,c("Curve")]),]
  ret <- subset(ret,R2 > 0.99)
  
  return(ret)
}


makeplots <- function(dir, data, colby) {
  
  pdf(paste0(dir,"/T_relations.pdf"))
  with(data,plot(Ts,Vcmax,col=colby,main="Vcmax"))
  with(data,plot(Ts,Jmax,col=colby,main="Jmax"))
  with(data,plot(Ts,TPU,col=colby,main="TPU"))
  with(data,plot(Ts,Rd,col=colby,main="Rday"))
  with(data,plot(Ts,resp,col=colby,main="A at Ci = 50 ppm"))
  with(data,plot(Ts,lowAs,col=colby,main="A at Ci = 300 ppm"))
  with(data,plot(Ts,highAs,col=colby,main="A at Ci = 800 ppm"))
  dev.off()
  
}

#----------------------------------------------------------------------------------------------------------------------

#function to fit linear mix models to estimate Topt for photosynthesis from quadratic equation

#---------------------------------------------------------------------------------------------------------------------
#dat=dataframe
#As=dependent variable (Photo, lowAs, highAs.....)
#rand=random effect

fit.boot<-function(dat,As,rand){
  
  q<-lmer(As~Ts+Tsq+(1|rand),data=dat)
  
  #function to get Topt for photosynthesis and Rate at Topt
  
  topt<-function(q){
    q.1<-summary(q)
    results<-(q.1$coefficients[1:3])
    Topt<--results[2]/2/results[3]
    Aopt<-results[1]+results[2]*Topt+results[3]*Topt^2
    
    
    return(c(Topt,Aopt))
  }
  
  #to get bootstrap SE
  bootfit2 <- bootMer(q, FUN=topt, re.form=NA,
                      nsim=999)
  
  #to get bootstrap CI
  
  lci <- apply(bootfit2$t, 2, quantile, 0.025)
  uci <- apply(bootfit2$t, 2, quantile, 0.975)
  
  return(list(bootfit2,lci,uci))
}

#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

fititd<-function(dat){
  try(An<-nls(Photo~(b*(Tleaf-Tmin)*((1-exp(c*(Tleaf-Tmax))))),data=dat,start=list(b=2,Tmin=0,Tmax=40,c=0.03)))
  A.1<-summary(An)
  results<-(A.1$coefficients[1:6])
  #names(results)[1:6] <- c("Aopt","Topt","b","Aopt.se","Topt.se","b.se")
  return(results)
  
}

#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

