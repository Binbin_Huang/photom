#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

#WTC3: E. tereticornis

#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------
Temps <- seq(15,40,by=0.1)
Tdew=15
VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)

#-parameters: Winter (Ambient temperature)

vcmax<-etret.vcmax[[1]][[3]]
eav<-etret.vcmax[[2]][[3]]*1000
delsv<-etret.vcmax[[3]][[3]]*1000

jmax<-etret.Jmax[[1]][[4]]
eaj<-etret.Jmax[[2]][[4]]*1000
delsj<-etret.Jmax[[3]][[4]]*1000

edvc=2e5
edvj=2e5


#Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
#Rd0<-1.6596714
#Q10<-2

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)



windows(50,50);par(mfrow=c(1,1))

plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="red",ylim=c(5,35),main="Ambient T")

legend("top",c("winter","summer","Autumn"),lwd=2,lty=c(2,2,1),col=c("red","forestgreen","black"),bty="n",ncol=3)

#-------------------------------------------------

#-parameters: Summer (Ambient temperature)

vcmax<-etret.vcmax[[1]][[2]]
eav<-etret.vcmax[[2]][[2]]*1000
delsv<-etret.vcmax[[3]][[2]]*1000

jmax<-etret.Jmax[[1]][[3]]
eaj<-etret.Jmax[[2]][[3]]*1000
delsj<-etret.Jmax[[3]][[3]]*1000

edvc=2e5
edvj=2e5


Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.4304719 #rate at 25C
Q10<-2

#with default Rday settings
sum.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)


lines(ALEAF~Tleaf,dat=sum.cold,type="l",lwd=2,col="forestgreen",ylim=c(5,35),main="Ambient T")

#---------------------------------------

#-parameters: Autumn (Ambient temperature)

vcmax<-etret.vcmax[[1]][[1]]
eav<-etret.vcmax[[2]][[1]]*1000
delsv<-etret.vcmax[[3]][[1]]*1000

jmax<-etret.Jmax[[1]][[1]]
eaj<-etret.Jmax[[2]][[1]]*1000
delsj<-etret.Jmax[[3]][[1]]*1000

edvc=2e5
edvj=2e5


#with default Rday settings
Autumn.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)


lines(ALEAF~Tleaf,dat=Autumn.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tereticornis, Summer/Ambient T")

An1<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=win.cold,start=list(Aopt=20,Topt=25,b=0.05))
An2<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=sum.cold,start=list(Aopt=20,Topt=25,b=0.05))
An3<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=Autumn.cold,start=list(Aopt=20,Topt=25,b=0.05))

#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

#fix Ci


vcmax<-etret.vcmax[[1]][[3]]
eav<-etret.vcmax[[2]][[3]]*1000
delsv<-etret.vcmax[[3]][[3]]*1000

jmax<-etret.Jmax[[1]][[4]]
eaj<-etret.Jmax[[2]][[4]]*1000
delsj<-etret.Jmax[[3]][[4]]*1000

edvc=2e5
edvj=2e5

ci<-275

#with default Rday settings
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Ci=ci)



windows(50,50);par(mfrow=c(1,1))

plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="red",ylim=c(5,35),main="Ambient T")

legend("top",c("winter","summer","Autumn"),lwd=2,lty=c(2,2,1),col=c("red","forestgreen","black"),bty="n",ncol=3)


An4<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=win.cold,start=list(Aopt=20,Topt=25,b=0.05))
An5<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=sum.cold,start=list(Aopt=20,Topt=25,b=0.05))
An6<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=Autumn.cold,start=list(Aopt=20,Topt=25,b=0.05))
#-------------------------------------------------

#-parameters: Summer (Ambient temperature)

vcmax<-etret.vcmax[[1]][[2]]
eav<-etret.vcmax[[2]][[2]]*1000
delsv<-etret.vcmax[[3]][[2]]*1000

jmax<-etret.Jmax[[1]][[3]]
eaj<-etret.Jmax[[2]][[3]]*1000
delsj<-etret.Jmax[[3]][[3]]*1000

edvc=2e5
edvj=2e5

Rd=0.728*exp(72.311/0.008314*(1/298-1/(Temps+273.15)))
Rd0<-1.4304719 #rate at 25C
Q10<-2

#with default Rday settings
sum.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Ci=ci)


lines(ALEAF~Tleaf,dat=sum.cold,type="l",lwd=2,col="forestgreen",ylim=c(5,35),main="Ambient T")

#---------------------------------------

#-parameters: Autumn (Ambient temperature)

vcmax<-etret.vcmax[[1]][[1]]
eav<-etret.vcmax[[2]][[1]]*1000
delsv<-etret.vcmax[[3]][[1]]*1000

jmax<-etret.Jmax[[1]][[1]]
eaj<-etret.Jmax[[2]][[1]]*1000
delsj<-etret.Jmax[[3]][[1]]*1000

edvc=2e5
edvj=2e5

gs<-0.25

#with default Rday settings
Autumn.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Ci=ci)


lines(ALEAF~Tleaf,dat=Autumn.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tereticornis, Summer/Ambient T")

#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------

#with seasonally Rday settings
#-parameters: Winter (Ambient temperature)

vcmax<-etret.vcmax[[1]][[3]]
eav<-etret.vcmax[[2]][[3]]*1000
delsv<-etret.vcmax[[3]][[3]]*1000

jmax<-etret.Jmax[[1]][[4]]
eaj<-etret.Jmax[[2]][[4]]*1000
delsj<-etret.Jmax[[3]][[4]]*1000

edvc=2e5
edvj=2e5

rd<-1.65

win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=rd)



windows(50,50);par(mfrow=c(1,1))

plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="red",ylim=c(5,35),main="Ambient T")

legend("top",c("winter","summer","Autumn"),lwd=2,lty=c(2,2,1),col=c("red","forestgreen","black"),bty="n",ncol=3)


-------------------------------------------------

#-parameters: Summer (Ambient temperature)
rd<-1.43

vcmax<-etret.vcmax[[1]][[2]]
eav<-etret.vcmax[[2]][[2]]*1000
delsv<-etret.vcmax[[3]][[2]]*1000

jmax<-etret.Jmax[[1]][[3]]
eaj<-etret.Jmax[[2]][[3]]*1000
delsj<-etret.Jmax[[3]][[3]]*1000

edvc=2e5
edvj=2e5


#with default Rday settings
sum.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=rd)


lines(ALEAF~Tleaf,dat=sum.cold,type="l",lwd=2,col="forestgreen",ylim=c(5,35))

#---------------------------------------

#-parameters: Autumn (Ambient temperature)



vcmax<-etret.vcmax[[1]][[1]]
eav<-etret.vcmax[[2]][[1]]*1000
delsv<-etret.vcmax[[3]][[1]]*1000

jmax<-etret.Jmax[[1]][[1]]
eaj<-etret.Jmax[[2]][[1]]*1000
delsj<-etret.Jmax[[3]][[1]]*1000

edvc=2e5
edvj=2e5

rd<-1.38
#with default Rday settings
Autumn.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Rd=rd)


lines(ALEAF~Tleaf,dat=Autumn.cold,type="l",lwd=2,col="black",ylim=c(5,35))

An7<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=win.cold,start=list(Aopt=20,Topt=25,b=0.05))
An8<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=sum.cold,start=list(Aopt=20,Topt=25,b=0.05))
An9<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=Autumn.cold,start=list(Aopt=20,Topt=25,b=0.05))
#

#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

#with fixed Vcmax settings (Fixed to winter values)

#-parameters: Winter (Ambient temperature)

vcmax<-etret.vcmax[[1]][[3]]
eav<-etret.vcmax[[2]][[3]]*1000
delsv<-etret.vcmax[[3]][[3]]*1000

jmax<-etret.Jmax[[1]][[4]]
eaj<-etret.Jmax[[2]][[4]]*1000
delsj<-etret.Jmax[[3]][[4]]*1000

edvc=2e5
edvj=2e5



win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)



windows(50,50);par(mfrow=c(1,1))

plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="red",ylim=c(5,35),main="Ambient T")

legend("top",c("winter","summer","Autumn"),lwd=2,lty=c(2,2,1),col=c("red","forestgreen","black"),bty="n",ncol=3)


-------------------------------------------------
#-parameters: Summer (Ambient temperature)
  

sum.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)


lines(ALEAF~Tleaf,dat=sum.cold,type="l",lwd=2,col="forestgreen",ylim=c(5,35))

#---------------------------------------

#-parameters: Autumn (Ambient temperature)

Autumn.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj)


lines(ALEAF~Tleaf,dat=Autumn.cold,type="l",lwd=2,col="black",ylim=c(5,35))

An10<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=win.cold,start=list(Aopt=20,Topt=25,b=0.05))
An11<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=sum.cold,start=list(Aopt=20,Topt=25,b=0.05))
An12<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=Autumn.cold,start=list(Aopt=20,Topt=25,b=0.05))
#

#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

#with fixed Vcmax settings (Fixed to winter values) and changed gs

#-parameters: Winter (Ambient temperature)

vcmax<-etret.vcmax[[1]][[3]]
eav<-etret.vcmax[[2]][[3]]*1000
delsv<-etret.vcmax[[3]][[3]]*1000

jmax<-etret.Jmax[[1]][[4]]
eaj<-etret.Jmax[[2]][[4]]*1000
delsj<-etret.Jmax[[3]][[4]]*1000

edvc=2e5
edvj=2e5

ci<-275
rd<-1.65
win.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,g1=2)



windows(50,50);par(mfrow=c(1,1))

plot(ALEAF~Tleaf,dat=win.cold,type="l",lwd=2,col="red",ylim=c(5,35),main="Ambient T")

legend("top",c("winter","summer","Autumn"),lwd=2,lty=c(2,2,1),col=c("red","forestgreen","black"),bty="n",ncol=3)


-------------------------------------------------
  #-parameters: Summer (Ambient temperature)
  rd<-3.5
  
  sum.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                       Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                       Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,g1=3)


lines(ALEAF~Tleaf,dat=sum.cold,type="l",lwd=2,col="forestgreen",ylim=c(5,35))

#---------------------------------------

#-parameters: Autumn (Ambient temperature)
rd<-1.38
Autumn.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,g1=4)


lines(ALEAF~Tleaf,dat=Autumn.cold,type="l",lwd=2,col="black",ylim=c(5,35))

An13<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=win.cold,start=list(Aopt=20,Topt=25,b=0.05))
An14<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=sum.cold,start=list(Aopt=20,Topt=25,b=0.05))
An15<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=Autumn.cold,start=list(Aopt=20,Topt=25,b=0.05))
#

#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

#Change only EaV

#-parameters: Summer (Ambient temperature)

vcmax<-etret.vcmax[[1]][[2]]
eav<-etret.vcmax[[2]][[2]]*1000
delsv<-etret.vcmax[[3]][[2]]*1000

jmax<-etret.Jmax[[1]][[3]]
eaj<-etret.Jmax[[2]][[3]]*1000
delsj<-etret.Jmax[[3]][[3]]*1000

edvc=2e5
edvj=2e5


#with default Rday settings
sum.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Ci=275)

windows(50,50);par(mfrow=c(1,1))
plot(ALEAF~Tleaf,dat=sum.cold,type="l",lwd=2,col="forestgreen",ylim=c(5,35),main="Ambient T/Changed EaV")

#---------------------------------------

#-parameters: Autumn (Ambient temperature)


eav<-etret.vcmax[[2]][[1]]*1000



#with default Rday settings
Autumn.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Ci=275)


lines(ALEAF~Tleaf,dat=Autumn.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tereticornis, Summer/Ambient T")



#-------------------------------
#-parameters: Winter (Ambient temperature)


eav<-etret.vcmax[[2]][[3]]*1000



#with default Rday settings
winter.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Ci=275)


lines(ALEAF~Tleaf,dat=winter.cold,type="l",lwd=2,col="red",ylim=c(5,35),main="E. tereticornis, Summer/Ambient T")



An16<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=sum.cold,start=list(Aopt=20,Topt=25,b=0.05))
An17<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=Autumn.cold,start=list(Aopt=20,Topt=25,b=0.05))
An18<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=winter.cold,start=list(Aopt=20,Topt=25,b=0.05))


legend("top",c("winter","summer","Autumn"),lwd=2,lty=c(2,2,1),col=c("red","forestgreen","black"),bty="n",ncol=3)


--
#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

#Change only EaJ

#-parameters: Summer (Ambient temperature)

vcmax<-etret.vcmax[[1]][[2]]
eav<-etret.vcmax[[2]][[2]]*1000
delsv<-etret.vcmax[[3]][[2]]*1000

jmax<-etret.Jmax[[1]][[3]]
eaj<-etret.Jmax[[2]][[3]]*1000
delsj<-etret.Jmax[[3]][[3]]*1000

edvc=2e5
edvj=2e5


#with default Rday settings
sum.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Ci=275)

windows(50,50);par(mfrow=c(1,1))
plot(ALEAF~Tleaf,dat=sum.cold,type="l",lwd=2,col="forestgreen",ylim=c(5,35),main="Ambient T/Changed EaJ")

#---------------------------------------

#-parameters: Autumn (Ambient temperature)


eaj<-etret.Jmax[[2]][[1]]*1000



#with default Rday settings
Autumn.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Ci=275)


lines(ALEAF~Tleaf,dat=Autumn.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tereticornis, Summer/Ambient T")



#-------------------------------
#-parameters: Winter (Ambient temperature)


eaj<-etret.vcmax[[2]][[4]]*1000



#with default Rday settings
winter.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,Ci=275)


lines(ALEAF~Tleaf,dat=winter.cold,type="l",lwd=2,col="red",ylim=c(5,35),main="E. tereticornis, Summer/Ambient T")



An19<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=sum.cold,start=list(Aopt=20,Topt=25,b=0.05))
An20<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=Autumn.cold,start=list(Aopt=20,Topt=25,b=0.05))
An21<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=winter.cold,start=list(Aopt=20,Topt=25,b=0.05))


legend("top",c("winter","summer","Autumn"),lwd=2,lty=c(2,2,1),col=c("red","forestgreen","black"),bty="n",ncol=3)


summary(An19)

#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------

#Change only g1

#-parameters: Summer (Ambient temperature)

vcmax<-etret.vcmax[[1]][[2]]
eav<-etret.vcmax[[2]][[2]]*1000
delsv<-etret.vcmax[[3]][[2]]*1000

jmax<-etret.Jmax[[1]][[3]]
eaj<-etret.Jmax[[2]][[3]]*1000
delsj<-etret.Jmax[[3]][[3]]*1000

edvc=2e5
edvj=2e5


#with default Rday settings
sum.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                     Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                     Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,g1=1.5)

windows(50,50);par(mfrow=c(1,1))
plot(ALEAF~Tleaf,dat=sum.cold,type="l",lwd=2,col="forestgreen",ylim=c(5,35),main="Ambient T/Changed EaJ")

#---------------------------------------

#-parameters: Autumn (Ambient temperature)



#with default Rday settings
Autumn.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,g1=4)


lines(ALEAF~Tleaf,dat=Autumn.cold,type="l",lwd=2,col="black",ylim=c(5,35),main="E. tereticornis, Summer/Ambient T")



#-------------------------------
#-parameters: Winter (Ambient temperature)




#with default Rday settings
winter.cold <- Photosyn(Tleaf=Temps,VPD=VPD,
                        Vcmax=vcmax,EaV=eav,EdVC=2e5,delsC=delsv,
                        Jmax = jmax,EaJ=eaj,EdVJ=2e5,delsJ=delsj,g1=4)


lines(ALEAF~Tleaf,dat=winter.cold,type="l",lwd=2,col="red",ylim=c(5,35),main="E. tereticornis, Summer/Ambient T")



An19<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=sum.cold,start=list(Aopt=20,Topt=25,b=0.05))
An20<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=Autumn.cold,start=list(Aopt=20,Topt=25,b=0.05))
An21<-nls(ALEAF~Aopt-(b*(Tleaf-Topt)^2),data=winter.cold,start=list(Aopt=20,Topt=25,b=0.05))


legend("top",c("winter","summer","Autumn"),lwd=2,lty=c(2,2,1),col=c("red","forestgreen","black"),bty="n",ncol=3)


summary(An19)

#-----------------------------------------------------------------------------------------------------------------------
 #-----------------------------------------------------------------------------------------------------------------------
