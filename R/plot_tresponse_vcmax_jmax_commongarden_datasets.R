
#---------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------



# Plot parameters of temperature response of Vcmax and Jmax (common garden datasets)
# Manuscript Figure 4 


#---------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------


  # Tresponse parameters for commongarden experiments
  tfits_VJ<-read.csv(paste0(path,"/Tables/biochemical_parameters_with_met_data_with_rd.csv"))
  tfits_VJ$MAXSS<-with(tfits_VJ,BIO5/10)
  adapt_bio<-subset(tfits_VJ,  DataSet %in% c("HFE_CG","DILLAWAY","S30","MEDLYN","DREYER","RF_MON",
                                              "MIKE_ASPINWALL","GREAT")) 
  adapt_bio<-adapt_bio[order(adapt_bio$DataSet,adapt_bio$Season),]
  adapt_bio$DataSet<-as.character(adapt_bio$DataSet)
  
  adapt_bio$DataSet[1:11]<-c(rep("DILLAWAY_1",4),rep("DILLAWAY_2",3),rep("DILLAWAY_3",4))
  
  adapt_bio.1<-subset(adapt_bio,!Season_New %in% c("Winter") & !Temp_Treatment %in% c(32,"Elevated",1,6))
  adapt_bio.1<-subset(adapt_bio.1,!Season %in% c("Jul","Jun","February","Nov","Mar","Oct")) #remove Jun n Jul data in Medlyn et al
  
  adapt_bio.1<-subset(adapt_bio.1,adapt_bio.1$Species!="Toona sinensis")
  adapt_bio.1$DataSet[adapt_bio.1$DataSet=="MARK_TJ"] <-"TJOELKER" #change just to keep color order
  
  adapt_bio.1$DataSet<-factor(adapt_bio.1$DataSet)
  
  
  #---------------------------------------------------------------------------------------------------------------------------------------
  #---------------------------------------------------------------------------------------------------------------------------------------
  
  COL.2<-palette(c("#B15928","#FFFF99","#FDBF6F","#E31A1C","darkorange1","#CAB2D6","#6A3D9A","#FB9A99","#33A02C","#B2DF8A","#1F78B4","#A6CEE3","#F781BF", "#A65628","#4DAF4A"))
  COL.2<-palette(c("#B15928","#FFFF99","#FDBF6F","#E31A1C","darkorange1","#CAB2D6","#6A3D9A","#FB9A99","#33A02C","#B2DF8A","#1F78B4","#A6CEE3","#F781BF", "#A65628","#4DAF4A"))
  
  #---------------------------------------------------------------------------------------------------------------------------------------
  #---------------------------------------------------------------------------------------------------------------------------------------
  
  #Vcmax and Jmax at 25C

  # png(filename="manuscript_figures/figure_3.png",width = 500, height = 500*1.6)
  # par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(4,2))
  
  pdf("manuscript_figures/figure_4.pdf",width = 5, height = 6)
  par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(3,2))
  
  
  #-----------------------------------
  #------------------------------------
  
  #- Topt for Vcmax
  
  with(adapt_bio.1,plot(BIO5/10,Topt_vcmax_PA,bg=COL.2[adapt_bio.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(20,55)))
  magaxis(side=c(1,2,4),labels=c(0,1,0),frame.plot=T,majorn =3)
  
  title(ylab=expression(Topt[V]~(degree*C)),
        xpd=NA,cex.lab=1.6)
  
  ebd.1<-subset(adapt_bio.1,adapt_bio.1$Topt_vcmax_SE_PA<10)
  adderrorbars(x=ebd.1$MAXSS,y=ebd.1$Topt_vcmax_PA,SE=ebd.1$Topt_vcmax_SE_PA ,direction="updown",col=COL.2[adapt_bio.1$DataSet])
  legend("topright",expression((bold(a))),bty="n",cex=1.5)
  with(adapt_bio.1,points(BIO5/10,Topt_vcmax_PA,bg=COL.2[adapt_bio.1$DataSet],col=COL.2[adapt_bio.1$DataSet],pch=21,cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(20,55)))
  
  
  lm_topt<-lmer(Topt_vcmax_PA~MAXSS+(MAXSS|DataSet),data=subset(adapt_bio.1,adapt_bio.1$DataSet!="MEDLYN"),
                weights=1/subset(adapt_bio.1,adapt_bio.1$DataSet!="MEDLYN")$Topt_vcmax_SE_PA)

  Anova(lm_topt)
  rsquared.glmm(lm_topt)
  
  # #--------------------------
  # #- add Kattge & Knorr model to the plot
  # ablineclip(a=24.92,b=0.44,x1=min(adapt_bio.1$gMDD0)-3,x2=max(adapt_bio.1$gMDD0)+3,lty = 3,lwd=2)
  # 
  # #--------------------------
  
  
  
  #- Topt for Jmax
  with(adapt_bio.1,plot(BIO5/10,Topt_jmax_PA,bg=COL.2[adapt_bio.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(20,55)))
  magaxis(side=c(1,2,4),labels=c(0,0,1),frame.plot=T,majorn =3)
  
  ebd.1<-subset(adapt_bio.1,adapt_bio.1$Topt_Jmax_SE_PA<10)
  adderrorbars(x=ebd.1$MAXSS,y=ebd.1$Topt_jmax_PA,SE=ebd.1$Topt_Jmax_SE_PA ,direction="updown",col=COL.2[adapt_bio.1$DataSet])
  
  
  title(ylab=expression(Topt[J]~(degree*C)),
        xpd=NA,cex.lab=1.6,line=-18)
  
  legend("topright",expression((bold(d))),bty="n",cex=1.5)
  
  
  # ablineclip(21.6094, 0.5738 , x1 = min(adapt_bio.1$MAXSS-2), x2 = max(adapt_bio.1$MAXSS+2),col="black",lwd=2,lty=1)
  
  # DataSet MEDLYN removed due to uncertainity in MAXSS (too high??)
  
  lm_toptj<-lmer(Topt_jmax_PA~MAXSS+(1|DataSet),data=subset(adapt_bio.1,adapt_bio.1$DataSet!="MEDLYN"),
                weights=1/subset(adapt_bio.1,adapt_bio.1$DataSet!="MEDLYN")$Topt_Jmax_SE_PA)
  
  Anova(lm_toptj)
  rsquared.glmm(lm_toptj)
  
  with(adapt_bio.1,points(BIO5/10,Topt_jmax_PA,bg=COL.2[adapt_bio.1$DataSet],col=COL.2[adapt_bio.1$DataSet],pch=21,cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(20,55)))
  
  
  # add regression line to the plot
  ablineclip(a= summary(lm_toptj)$coefficients[1],b=summary(lm_toptj)$coefficients[2],x1=min(adapt_bio.1$MAXSS)-2,
             x2=max(adapt_bio.1$MAXSS)+2,
             y1=min(adapt_bio.1$Topt_jmax_PA,na.rm=T)-2,y2=max(adapt_bio.1$Topt_jmax_PA,na.rm=T)+2,
             lty=1,col="black",lwd=2)
  
  
  
  #-----------------------------------
  #------------------------------------
  
  # Ea for Vcmax and Jmax
  
  #- Ea Vcmax
  with(adapt_bio.1,plot(BIO5/10,EaV,bg=COL.2[adapt_bio.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(0,150)))
  magaxis(side=c(1,2,4),labels=c(0,1,0),frame.plot=T,majorn = 3)
  ea.se<-subset(adapt_bio.1,adapt_bio.1$EaV.se<10 & adapt_bio$EaV<150) # remove few high se values to improve the clarity
  
  adderrorbars(x=adapt_bio.1$BIO5/10,y=adapt_bio.1$EaV,SE=ea.se$EaV.se ,direction="updown",col=COL.2[adapt_bio.1$DataSet])
  
  title(ylab=expression(Ea[V]~(kJ~mol^-1)),
        xpd=NA,cex.lab=1.6)
  legend("topright",expression((bold(b))),bty="n",cex=1.5)
  
  with(adapt_bio.1,points(BIO5/10,EaV,bg=COL.2[adapt_bio.1$DataSet],col=COL.2[adapt_bio.1$DataSet],pch=21,cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(20,150)))
  
  #-----------------------------------
  #------------------------------------
  
  #- Ea Jmax
  with(adapt_bio.1,plot(BIO5/10,EaJ,bg=COL.2[adapt_bio.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(0,150)))
  magaxis(side=c(1,2,4),labels=c(0,0,1),frame.plot=T,majorn = 3)
  adderrorbars(x=adapt_bio.1$BIO5/10,y=adapt_bio.1$EaJ,SE=ea.se$EaJ.se ,direction="updown",col=COL.2[adapt_bio.1$DataSet])
  title(ylab=expression(Ea[J]~(kJ~mol^-1)),
        xpd=NA,cex.lab=1.6,line=-18)
  legend("topright",expression((bold(e))),bty="n",cex=1.5)
  
  with(adapt_bio.1,points(BIO5/10,EaJ,bg=COL.2[adapt_bio.1$DataSet],col=COL.2[adapt_bio.1$DataSet],pch=21,cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(0,150)))
  
  #-----------------------------------
  #------------------------------------
  
  
  #- deltaS vcmax
  
  with(adapt_bio.1,plot(BIO5/10,delsV*1000,bg=COL.2[adapt_bio.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(600,670)))
  magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,majorn =3)
  title(ylab=expression(Delta*S[V]~(J~mol^-1~K^-1)),
        xpd=NA,cex.lab=1.6)
  
  ebd.1<-subset(adapt_bio.1,adapt_bio.1$delsV.se<0.03)
  adderrorbars(x=ebd.1$MAXSS,y=ebd.1$delsV*1000,SE=ebd.1$delsV.se*1000 ,direction="updown",col=COL.2[adapt_bio.1$DataSet])
  legend("topright",expression((bold(c))),bty="n",cex=1.5)
  
  with(adapt_bio.1,points(BIO5/10,delsV*1000,bg=COL.2[adapt_bio.1$DataSet],col=COL.2[adapt_bio.1$DataSet],pch=21,cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(600,670)))
  
  #--------------------------
  #- add Kattge & Knorr model to the plot
  # ablineclip(a=668.39,b=-1.07,x1=min(adapt_bio.1$MAXSS)-3,x2=max(adapt_bio.1$MAXSS)+3,lty = 3,lwd=2)
  
  #--------------------------
  # lm_dsv<-lmer(delsV~MAXSS+(MAXSS|DataSet),data=adapt_bio.1,
  #                weights=1/adapt_bio.1$delsV.se)
  # 
  # Anova(lm_dsv)
  # rsquared.glmm(lm_dsv)
  # 
  #---------------------------------------------------------------------------------------------------
  
  #- deltaS Jmax
  
  with(adapt_bio.1,plot(BIO5/10,delsJ*1000,bg=COL.2[adapt_bio.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(600,670)))
  magaxis(side=c(1,2,4),labels=c(1,0,1),frame.plot=T,majorn =3)
  
  ebd.1<-subset(adapt_bio.1,adapt_bio.1$delsJ.se<0.03)
  adderrorbars(x=ebd.1$MAXSS,y=ebd.1$delsJ*1000,SE=ebd.1$delsJ.se*1000 ,direction="updown",col=COL.2[adapt_bio.1$DataSet])
  legend("topright",expression((bold(f))),bty="n",cex=1.5)
  
  
  title(ylab=expression(Delta*S[J]~(J~mol^-1~K^-1)),
        xpd=NA,cex.lab=1.6,line=-18)
  
  
  # ablineclip(662.34, -0.97, x1 = min(adapt_bio.1$MAXSS-2), x2 = max(adapt_bio.1$MAXSS+2),col="black",lwd=2,lty=1)
  lm_dsj<-lmer(delsJ~MAXSS+(1|DataSet),data=subset(adapt_bio.1,adapt_bio.1$DataSet!="MEDLYN"),
               weights=1/subset(adapt_bio.1,adapt_bio.1$DataSet!="MEDLYN")$delsJ.se)
  
  Anova(lm_dsj)
  rsquared.glmm(lm_dsj)
  
  
  with(adapt_bio.1,points(BIO5/10,delsJ*1000,bg=COL.2[adapt_bio.1$DataSet],col=COL.2[adapt_bio.1$DataSet],pch=21,cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,40),ylim=c(600,670)))
  ablineclip(662.25, -0.999, x1 = min(adapt_bio.1$MAXSS-2), x2 = max(adapt_bio.1$MAXSS+2),col="black",lwd=2,lty=1)
  
  
  # #--------------------------
  # #- add Kattge & Knorr model to the plot
  # ablineclip(a=659.70,b=-0.75,x1=min(adapt_bio.1$gMDD0)-3,x2=max(adapt_bio.1$gMDD0)+3,lty = 3,lwd=2)
  # 
  # #--------------------------
  
  
 
  
  
  # #--------------------------
  # #- add Kattge & Knorr model to the plot
  # ablineclip(a=26.21,b=0.33,x1=min(adapt_bio.1$gMDD0)-3,x2=max(adapt_bio.1$gMDD0)+3,lty = 3,lwd=2)
  # 
  # #--------------------------
  # 
  
  title(xlab=expression(~T[home]~(degree*C)),cex.lab=1.6,outer=T,adj=0.175,line=2)
  title(xlab=expression(~T[home]~(degree*C)),cex.lab=1.6,outer=T,adj=0.825,line=2)
  
  
  # title(xlab=expression(Home~T[air]~max~(degree*C)),cex.lab=1.5,outer=T,adj=0.5,line=3)
  
  #------------------------------------------------------------------------------------------------------------------------------
  #------------------------------------------------------------------------------------------------------------------------------
  
  dev.off()
