#
#plot species measured at their home climates
#########################################################################################################################################
#Read biochemical data

tfits_VJ<-read.csv(paste0(path,"/Tables/biochemical_parameters_with_met_data.csv"))
t_home_bio<-subset(tfits_VJ,  DataSet %in% c("GWW","RF_AMZ","SAVANNA","TARVAINEN","TMB",
                                             "WANG_ET_AL","RF_AUS","HAN_2","Martijn_Slot","ARCTIC","EucFace","ANNA","Alida Mao","TARVAINEN_B",
                                             "Kelsey_Cater","Hikosaka","ELLSWORTH","MEDLYN")) 

t_home_bio<-subset(t_home_bio,!t_home_bio$Species %in% c("Pinus pinaster_T","Betula pendula") 
                   & !t_home_bio$Season %in% c("WINTER","winter","dry",4,10,"June","August","Oct",
                                               "Jan","Jul","Mar","Nov","Jun"))

#t_home_bio<-t_home_bio[-14,] # remove Hikosaka september data 

t_home_bio$DataSet<-factor(t_home_bio$DataSet)

t_home_bio<-t_home_bio[with(t_home_bio, order(PFT, DataSet)), ]

t_home_bio$num <- ave( t_home_bio$Vcmax25, t_home_bio$PFT,FUN = seq_along) # set  unique numbers for colors

t_home_bio$Topt_jmax<-ifelse(is.na(t_home_bio$Topt_jmax),t_home_bio$ToptJ,t_home_bio$Topt_jmax)

t_home_bio$delsv.new<-t_home_bio$delsV*1000
t_home_bio$delsj.new<-t_home_bio$delsJ*1000

###########################################################################################################################################
# with(tfits_VJ,plot(delsJ,delsJ_pred,xlim=c(.6,.7),ylim=c(.6,.7)))
# abline(0,1)
# #-----------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------


# #Read biochemical topt data
# 
# tfits_VJ_topts<-read.csv(paste0(path,"/Tables/biochemical_topts_with_met_data.csv"))
# 
# t_home_bio_topts<-subset(tfits_VJ_topts,  DataSet %in% c("GWW","RF_AMZ","SAVANNA","TARVAINEN","TMB",
#                                                          "WANG_ET_AL","RF_AUS","EucFace","ANNA","Alida Mao","Kelsey_Cater","Hikosaka","ELLSWORTH","MEDLYN")) 
# 
# t_home_bio_topts<-subset(t_home_bio_topts,!t_home_bio_topts$Species %in% c("Pinus pinaster_T","Betula pendula") 
#                          & !t_home_bio_topts$Season %in% c("WINTER","winter","dry","Nov",4,8,"June","August",
#                                                            "AugSept","Jan","Jul","Mar","Nov","Jun"))
# 
# #t_home_bio_topts<-t_home_bio_topts[-14,] # remove Hikosaka september data 
# t_home_bio_topts<-subset(t_home_bio_topts,!is.na(t_home_bio_topts$Vcmax_opt))
# t_home_bio_topts$DataSet<-factor(t_home_bio_topts$DataSet)
# 
# #t_home_bio_topts$num <- ave( t_home_bio_topts$Topt_vcmax, t_home_bio_topts$PFT,FUN = seq_along) # set  unique numbers for colors
# 
# 
# t_home_bio<-merge(t_home_bio_topts[c(1:17)],t_home_bio,by=c("Species","DataSet"),all=T)
# t_home_bio$num <- ave( t_home_bio$Vcmax25, t_home_bio$PFT,FUN = seq_along) # set  unique numbers for colors
# t_home_bio<-t_home_bio[-c(87:98,100:111)]
# 
# # make unique color codes
# t_home_bio$num<-ifelse(t_home_bio$Species=="Puerto_Rico_Tropical_Spp_1",4,
#                            ifelse(t_home_bio$Species=="Puerto_Rico_Tropical_Spp_2",5,
#                                   ifelse(t_home_bio$Species=="RF_AUSTRALIA",6,t_home_bio$num)))
# ###########################################################################################################################################


#-----------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------

#########################################################################################################################################
###########################################################################################################################################

#Figure 1
#plot species measured at their home climates
path<-getwd()
tfits<-read.csv(paste0(path,"/Tables/netphoto_parameters_with_met_data.csv"))

t_home<-subset(tfits,  DataSet %in% c("GWW","RF_AMZ","SAVANNA","TARVAINEN","TMB",
                                      "WANG_ET_AL","RF_AUS","HAN_2","Martijn_Slot","ARCTIC","EucFace","ANNA","Alida Mao","TARVAINEN_B",
                                      "Kelsey_Cater","Hikosaka","ELLSWORTH","MEDLYN")) #these datasets are from

t_home.1<-subset(t_home,!t_home$Species %in% c("Pinus pinaster_T","Betula pendula") & !t_home$Season %in% c("WINTER","winter","dry","Nov",4,10,"June","August",
                                                                                                            "Mar","Jan","Jul","AugSept","Nov"))


t_home.1<-t_home.1[with(t_home.1, order(PFT, DataSet)), ]

t_home.1$DataSet<-factor(t_home.1$DataSet)

t_home.1$num <- ave( t_home.1$Topt, t_home.1$PFT,FUN = seq_along) # set  unique numbers for colors


library(RColorBrewer)

# set the color palette
colourCount<-length(unique(t_home.1$Species))+1
getPalette<-colorRampPalette(brewer.pal(6,"Set1"))
COL<-getPalette(6)

windows(50,50);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,0),cex.axis=1.5,las=1,mfrow=c(1,1))

# pdf(file="Figure2-Photo_vs_Temperature.pdf",width=4,height=4)
# par(mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,0))

#with(t_home.1,plot(MAXSS,Topt,col=COL[factor(DataSet)],pch=16,cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,40),ylim=c(10,35)))
#with(t_home.1,plot(gMDD0,Topt,bg=COL[num],pch=c(21,22,23,24,25,16)[t_home.1$PFT],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(10,45)))

with(t_home.1,plot(gMDD0,Topt,bg=COL[PFT],pch=c(21,22,23,24,25)[t_home.1$num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(10,45)))

ae<-subset(t_home.1,t_home.1$Topt.se<8) #remove one Se=9.4 to improve the clarity
adderrorbars(x=t_home.1$gMDD0,y=t_home.1$Topt,SE=ae$Topt.se ,direction="updown")

#adderrorbars(x=t_home.1$MAXSS,y=t_home.1$Topt,SE=ae$Topt.se ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)


# # legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
# #                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
# #                    "Australian rainforest"),pt.bg =COL[t_home.1$num],pch=c(21,22,23,24,25,16)[t_home.1$PFT],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")
# 
# 
# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")


legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")


#title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=2,outer=T)
title(ylab=expression(Topt[A]~(degree*C)),
      xpd=NA,cex.lab=2)
title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=2,outer=T)

lm_topt<-lm(Topt~gMDD0,data=t_home.1)
summary(lm_topt)$coefficients

plot_fit_line(data=t_home.1,yvar="Topt",xvar="gMDD0",linecol="black",fitoneline=T)

text(28,12,expression(T[optA]==13.91+0.61~T[Home])) #Coefficients and R2 updated after adding new datasets
text(28,11,expression(R^2==0.84)) 


#-----------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------

#plot Vcmax and Jmax at 25C

windows(100,60);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(1,2))

with(t_home_bio,plot(gMDD0,Vcmax25,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(10,250)))


adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$Vcmax25,SE=t_home_bio$Vcmax25.se ,direction="updown")
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(ylab=expression(V[cmax]~(mu*mol~m^-2~s^-1)),
      xpd=NA,cex.lab=1.5)


# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")



legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")

with(t_home_bio,plot(gMDD0,Jmax25,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(10,250)))

adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$Jmax25,SE=t_home_bio$Jmax25.se ,direction="updown")
magaxis(side=c(1,2,4),labels=c(1,0,1),frame.plot=T)
title(ylab=expression(J[max]~(mu*mol~m^-2~s^-1)),
      xpd=NA,cex.lab=1.5,line=-27)

title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.2,line=3)

title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.9,line=3)

# linear regression

lm_vcmax<-lm(Vcmax25~gMDD0,data=t_home_bio)
summary(lm_vcmax)


lm_jmax<-lm(Jmax25~gMDD0,data=t_home_bio)
summary(lm_jmax)

plot_fit_line(data=t_home_bio,yvar="Jmax25",xvar="gMDD0",linecol="black",fitoneline=T)

text(25,250,expression(J[max]==168.37-3.38~T[Home])) #Coefficients and R2 updated after adding new datasets
text(25,240,expression(R^2==0.33)) 


#-----------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------

#plot activation energies for Vcmax and Jmax

windows(100,60);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(1,2))

with(t_home_bio,plot(gMDD0,EaV,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,200)))

# ae<-subset(t_home_bio,t_home_bio$EaV.se<50) #remove one Se=9.4 to improve the clarity
# adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$EaV,SE=ae$EaV.se ,direction="updown")


adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$EaV,SE=t_home_bio$EaV.se ,direction="updown")
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(ylab=expression(E[a(Vcmax)]~(kJ~mol^-1)),
      xpd=NA,cex.lab=1.5)

# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")


legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")


with(t_home_bio,plot(gMDD0,EaJ,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,200)))
adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$EaJ,SE=t_home_bio$EaJ.se ,direction="updown")
magaxis(side=c(1,2,4),labels=c(1,0,1),frame.plot=T)

title(ylab=expression(E[a(Jmax)]~(kJ~mol^-1)),
      xpd=NA,cex.lab=1.5,line=-27)
title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.2,line=3)

title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.9,line=3)


#test linear regression with gMDD0

lm_eav<-lm(EaV~gMDD0,data=t_home_bio)
summary(lm_eav)


lm_eaj<-lm(EaJ~gMDD0,data=t_home_bio)
summary(lm_eaj)

#only intercept significant. slopes were not significant in both cases

#--------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------

windows(60,60);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(1,1))

with(t_home_bio,plot(EaV,Topt_275,bg=COL[num],pch=c(21,22,23,24,25)[PFT],cex=1.5,ylab="",xlab="",axes=F,xlim=c(30,120),ylim=c(15,40)))

# ae<-subset(t_home_bio,t_home_bio$EaV.se<50) #remove one Se=9.4 to improve the clarity
# adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$EaV,SE=ae$EaV.se ,direction="updown")


adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$EaV,SE=t_home_bio$EaV.se ,direction="updown")
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(ylab=expression(E[a(Vcmax)]~(kJ~mol^-1)),
      xpd=NA,cex.lab=1.5)


# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")



legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")




#-----------------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#subset(t_home_bio,!t_home_bio$DataSet %in% c("ARCTIC","HAN_2"))
#plot topt for vcmax and jmax, dels for vcmax and jmax



windows(100,60);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(1,2))

#- dels Vcmax
with(t_home_bio,plot(gMDD0,delsV*1000,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(600,700)))

ae.1<-subset(t_home_bio,t_home_bio$delsV.se<.03) #remove two SEs to improve the clarity

adderrorbars(x=t_home_bio$gMDD0,
             y=t_home_bio$delsV*1000,
             SE=ae.1$delsV.se*1000 ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(ylab=expression(Delta*S[Vcmax]~(J~mol^-1~K^-1)),
      xpd=NA,cex.lab=1.5)


text(25,610,expression(Delta*S[Vcmax]==659.57-1.17~T[Home])) #95%CI
text(25,605,expression(R^2==0.61)) 

#add Tumbarumba  delsv
# points(8.5,tmb_sum[[3]]*1000,pch=16,cex=2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(620,680),col=COL[6])
# adderrorbars(x=8.5,y=tmb_sum[[3]]*1000,SE=tmb_sum[[6]]*1000 ,direction="updown")

plot_fit_line(t_home_bio,yvar="delsv.new",xvar="gMDD0",linecol="black",fitoneline=T)


# 
# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")


legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")

#--------------------------
#- add Kattge & Knorr model to the plot
ablineclip(a=668.39,b=-1.07,x1=min(t_home_bio$gMDD0),x2=max(t_home_bio$gMDD0),lty = 2)

#--------------------------

#- dels Jmax
with(t_home_bio,plot(gMDD0,delsJ*1000,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(600,700)))

ae<-subset(t_home_bio,t_home_bio$delsJ.se<.04) #remove two SEs to improve the clarity
adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$delsJ*1000,SE=ae$delsJ.se*1000 ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,0,1),frame.plot=T)
title(ylab=expression(Delta*S[Jmax]~(J~mol^-1~K^-1)),
      xpd=NA,cex.lab=1.5,line=-27)

plot_fit_line(t_home_bio,yvar="delsj.new",xvar="gMDD0",linecol="black",fitoneline=T)


title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.2,line=3)

title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.9,line=3)

text(25,610,expression(Delta*S[Jmax]==661.47-1.12~T[Home])) #95%CI
text(25,605,expression(R^2==0.39)) 

#--------------------------
#- add Kattge & Knorr model to the plot
ablineclip(a=659.70,b=-0.75,x1=min(t_home_bio$gMDD0),x2=max(t_home_bio$gMDD0),lty = 2)

#--------------------------

t_home_bio$MAXSS<-with(t_home_bio,BIO5/10)
#linear regressin of delta S 

# for Vcmax
lm_delsv<-lm(delsv.new~gMDD0,data=t_home_bio)
summary(lm_delsv)

lm_delsj<-lm(delsj.new~gMDD0,data=t_home_bio)
summary(lm_delsj)

# regression with MAXSS

lm_delsj<-lm(delsj.new~MAXSS,data=t_home_bio)
summary(lm_delsj)

lm_delsv<-lm(delsv.new~MAXSS,data=t_home_bio)
summary(lm_delsv)

###########################################################################################################################################


#- Topt for Vcmax

windows(100,60);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(1,2))


with(t_home_bio,plot(gMDD0,Topt_vcmax,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(20,60)))
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(ylab=expression(Topt[Vcmax]~(degree*C)),
      xpd=NA,cex.lab=1.5)


adderrorbars(x=t_home_bio$gMDD0,
             y=t_home_bio$Topt_vcmax,
             SE=t_home_bio$Topt_vcmax_SE ,direction="updown")

plot_fit_line(t_home_bio,yvar="Topt_vcmax",xvar="gMDD0",linecol="black",fitoneline=T)

# linear regression with gMDD0

lm_toptv<-lm(Topt_vcmax~gMDD0,data=t_home_bio)
summary(lm_toptv)

text(25,23,expression(Topt[Vcmax]==25.37+0.64~T[Home])) 
text(25,21,expression(R^2==0.68)) 

#--------------------------
#- add Kattge & Knorr model to the plot
ablineclip(a=24.92,b=0.44,x1=min(t_home_bio$gMDD0),x2=max(t_home_bio$gMDD0),lty = 2)

#--------------------------

#add Tumbarumba  Toptv
#points(8.5,tmb_sum[[7]],pch=16,cex=2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(20,45),col=COL[6])


#- Topt for Jmax
with(t_home_bio,plot(gMDD0,Topt_jmax,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(20,60)))


magaxis(side=c(1,2,4),labels=c(1,0,1),frame.plot=T)
title(ylab=expression(Topt[Jmax]~(degree*C)),
      xpd=NA,cex.lab=1.5,line=-27)

ae<-subset(t_home_bio,t_home_bio$Topt_Jmax_SE<15) #remove two SEs to improve the clarity

adderrorbars(x=ae$gMDD0,
             y=ae$Topt_jmax,
             SE=ae$Topt_Jmax_SE,direction="updown")

#points(8.5,tmb_sum.j[[7]],pch=16,cex=2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(20,45),col=COL[6])
plot_fit_line(t_home_bio,yvar="Topt_jmax",xvar="gMDD0",linecol="black",fitoneline=T)

lm_toptj<-lm(Topt_jmax~gMDD0,data=t_home_bio)
summary(lm_toptj)

text(25,23,expression(Topt[Jmax]==23.56+0.58~T[Home])) 
text(25,21,expression(R^2==0.52)) 

#--------------------------
#- add Kattge & Knorr model to the plot
ablineclip(a=26.21,b=0.33,x1=min(t_home_bio$gMDD0),x2=max(t_home_bio$gMDD0),lty = 2)

#--------------------------


title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.2,line=3)

title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.9,line=3)

# # 

# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")

legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")

---------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------


#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------

#- topt at a common Ci

t_home_bio$JVr_Es<-with(t_home_bio,Jmax25/Vcmax25)
t_home_bio$JVr<-ifelse(is.na(t_home_bio$JVr),t_home_bio$JVr_Es,t_home_bio$JVr)


windows(60,60);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,0),cex.axis=1.5,las=1,mfrow=c(1,1))


with(t_home_bio,plot(gMDD0,Topt_275,bg=COL[PFT],pch=c(21,22,23,24,25)[t_home_bio$num],
                                                                       cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(10,45)))
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,at=1:13)

title(ylab=expression(Topt[A275]~(degree*C)),
      xpd=NA,cex.lab=2)

title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=2,outer=T,line=3)


adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$Topt_275,SE=t_home_bio$Topt_275.se ,direction="updown")


lm_fit<-lm(Topt_275~gMDD0,data=t_home_bio)
summary(lm_fit)

plot_fit_line(t_home_bio,yvar="Topt_275",xvar="gMDD0",linecol="black",fitoneline=T)

#abline(a=coef(lm_fit)[1],b=coef(lm_fit)[2],lty=3,lwd=2)

text(25,12,expression(Topt[A275]==17.21+0.57~T[Home])) 
text(25,10,expression(R^2==0.74)) 


# # 

# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")

legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")

---------------------------
  #------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------

# Jmax:Vcmax Ratio

windows(60,60);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,0),cex.axis=1.5,las=1,mfrow=c(1,1))


with(t_home_bio,plot(gMDD0,JVr,bg=COL[PFT],pch=c(21,22,23,24,25)[num],
                         cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,4)))
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,at=1:13)



# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")

legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")


#with(t_home_bio,plot(gMDD0,JVr_Es,col=COL[factor(DataSet)],pch=16,cex=2,ylab="",xlab="",axes=F,xlim=c(-15,35),ylim=c(0,3)))

adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$JVr,SE=t_home_bio$JVr_SE ,direction="updown")
title(ylab=expression(JV[ratio~at~25~degree*C]),
      xpd=NA,cex.lab=2)

#with(subset(t_home_bio,t_home_bio$Species %in% c("Eucalyptus salmonophloia")),points(gMDD0,Jmax25/Vcmax25
# ,col=COL[c(1,4)],pch=16,cex=2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,3)))

title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=2,outer=T)

 
plot_fit_line(subset(t_home_bio,!t_home_bio$DataSet %in% c("WANG_ET_AL")),yvar="JVr",xvar="gMDD0",linecol="black",fitoneline=T)

text(25,0.3,expression(JV[r]==2.61-0.050~T[Home])) 
text(25,0.1,expression(R^2==0.63)) 

# without ,"Kelsey_Cater", R2=0.36 otherwise not significant

lm_fit_JVr<-lm(JVr~gMDD0,data=subset(t_home_bio,!t_home_bio$DataSet %in% c("WANG_ET_AL","Kelsey_Cater")))
summary(lm_fit_JVr)


#--------------------------
#- add Kattge & Knorr model to the plot
ablineclip(a=2.59,b=-0.035,x1=min(t_home_bio$gMDD0),x2=max(t_home_bio$gMDD0),lty = 2)

#--------------------------

#------------------------------------------------------------

#Aratio

windows(60,60);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,0),cex.axis=1.5,las=1,mfrow=c(1,1))


with(t_home_bio,plot(gMDD0,A_ratio,bg=COL[PFT],pch=c(21,22,23,24,25)[num],
                         cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,5)))
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,at=1:13)


adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$A_ratio,SE=t_home_bio$A_ratio_SE ,direction="updown")

title(ylab=expression(A[ratio~at~25~degree*C]),
      xpd=NA,cex.lab=2)



# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")
# 
legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")




#with(subset(t_home_bio,t_home_bio$Species %in% c("Eucalyptus salmonophloia")),points(gMDD0,Jmax25/Vcmax25
# ,col=COL[c(1,4)],pch=16,cex=2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,3)))

title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=2,outer=T)

lm_fit_Ar<-lm(A_ratio~gMDD0,data=subset(t_home_bio,!t_home_bio$DataSet %in% c("Kelsey_Cater")))
summary(lm_fit_Ar)

plot_fit_line(subset(t_home_bio,!t_home_bio$DataSet %in% c("Kelsey_Cater")),yvar="A_ratio",xvar="gMDD0",linecol="black",fitoneline=T)

text(25,0.3,expression(A[ratio]==2.05-0.025~T[Home])) 
text(25,0.1,expression(R^2==0.24)) 

#-----------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------

# Day Respitation

windows(60,60);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,0),cex.axis=1.5,las=1,mfrow=c(1,1))


with(t_home_bio,plot(gMDD0,Rday,bg=COL[PFT],pch=c(21,22,23,24,25)[num],
                         cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,5)))
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,at=1:13)

title(ylab=expression(R[day~at~25~degree*C]),
      xpd=NA,cex.lab=2)

title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=2,outer=T)

adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$Rday,SE=t_home_bio$Rday_SE ,direction="updown")


lm_rday<-lm(Rday~gMDD0,data=subset(t_home_bio,!t_home_bio$DataSet %in% c("WANG_ET_AL")))
summary(lm_rday)

plot_fit_line(data=t_home_bio,yvar="Rday",xvar="gMDD0",linecol="black",fitoneline=T)

text(25,5,expression(R[day]==2.4-0.05~T[Home])) 
text(25,4.6,expression(R^2==0.26)) 

#-----------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------


# stomatal conductance (g1)

windows(50,50);
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,0),cex.axis=1.5,las=1,mfrow=c(1,1))

#with(t_home.1,plot(MAXSS,Topt,col=COL[factor(DataSet)],pch=16,cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,40),ylim=c(10,35)))

with(t_home.1,plot(gMDD0,g1,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,10)))


adderrorbars(x=t_home.1$gMDD0,y=t_home.1$g1,SE=t_home.1$g1.se ,direction="updown")

#adderrorbars(x=t_home.1$MAXSS,y=t_home.1$Topt,SE=ae$Topt.se ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)



# legend("topleft",c("Brazilian rainforest","Arctic spp","Australian semi-arid woodlands","Alpine Ash","Forest Red Gum","Australian savanna", "Panamanian rainforest","Spruce (Sweden)",
#                    "Black Spruce (Minnesota)","Japanese Red Pine","Maritime pine","Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Puerto Rico Tropical spp (a)","Puerto Rico Tropical spp (b)","Mongolian oak (Japan)",
#                    "Australian rainforest"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")

legend("topleft",c("Mongolian oak (Japan)","Forest Red Gum","Australian semi-arid woodlands","Australian savanna","Alpine Ash", "Black Spruce (Minnesota)","Spruce (Sweden)",
                   "Scots pine (Sweden)","Scots pine (Finland)","Duke pine","Japanese Red Pine","Maritime pine","Puerto Rico Tropical spp (b)","Puerto Rico Tropical spp (a)",
                   "Panamanian rainforest","Brazilian rainforest","Australian rainforest","Arctic spp"),pt.bg =COL[t_home.1$PFT],pch=c(21,22,23,24,25)[t_home.1$num],ncol=2,cex=.8,pt.cex=1.5,pt.lwd=1,title="Dataset",bty="n")




#title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=2,outer=T)
title(ylab=expression(g[1]~(kPa^-1)),
      xpd=NA,cex.lab=2)
title(xlab=expression(Home~T[air]~(degree*C)),cex.lab=2,outer=T)

lm_g1<-lm(g1~gMDD0,data=t_home.1)
summary(lm_g1)

# plot_fit_line(data=t_home.1,yvar="Topt",xvar="gMDD0",linecol="black",fitoneline=T)
# 
# text(28,12,expression(T[optA]==13.32+0.64~T[Home])) #Coefficients and R2 updated after adding new datasets
# text(28,11,expression(R^2==0.85)) 

