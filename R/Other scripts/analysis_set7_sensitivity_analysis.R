#---------------------------------------------------------------------------------------------------------------------
#- use temperature acclimation and adaptation functions to derive Topt for photosynthesis at a common Ci
#- model used: Photosyn (plantecophys package); coupled photosynthesis-stomatal conductance model

# read biochemical parameters

tfits_VJ<-read.csv(paste0(path,"/Tables/biochemical_parameters_with_met_data.csv"))

# calculate maximum temperature of the warmest quarter
tfits_VJ$MAXSS<-with(tfits_VJ,BIO5/10)

# for tropical species, Tgrowth is not available in some cases. So set to long-term mean temperature as there is no significant
# seasonal effects

tfits_VJ$Tavg_30<-ifelse(is.na(tfits_VJ$Tavg_30),tfits_VJ$gMDD0,tfits_VJ$Tavg_30)


# get the difference between long-term average growing season temperature and growth temperature
# acclimation to growth temperature is based on the difference


tfits_VJ$Tdiff<-with(tfits_VJ,Tavg_30-gMDD0)

tfits_VJ$Tdiff_m<-with(tfits_VJ,Tavg_30-MAXSS)



# function to model photosynthesis 
# only deltas for Jmax changed



model_photo_newf<-function(data){
  
  # get a sequence of leaf temperatures
  Temps <- seq(15,40,by=0.1)
  Tdew=15
  VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)
  
  
  # Vcmax - PFT specific
  Vcmax25<-data$Vcmax25
  
  
  # Activation energy for Jmax, PFT specific
  
  EaJ<-data$EaJ*1000
  
  # deltaS for Vcmax, PFT specific
  delsV<-data$delsV*1000
  #delsV<-with(data,(661.24-(1.24*gMDD0)))
  
  
  #dS for Jmax
  #both adaptationa and acclimation included. when Tgrowth > Thome, dS decrease and vise versa
  
  delsJ<-with(data,662.7-(0.97*MAXSS)+(-.562*Tdiff_m)) # adaptation from commongarden experiments
  
  # Jmax:Vcmax at 25C
  #both adaptationa and acclimation included. when Tgrowth > Thome, JVr decrease and vise versa
  jvr<-with(data,2.4-(0.021607*MAXSS)+(-0.02762*Tdiff_m)) # adaptation from commongarden experiments
  
  # get Jmax as a function of Vcmax
  Jmax25<-data$Jmax25
  
  # Activation energy for Vcmax
  # accimated for growth temperature
  
  #EaV<-with(data,EaV+(1.14*Tdiff))*1000
  
  EaV<-data$EaV*1000
  
  # model photosynthesis at a common Ci=275 ppm
  out <- Photosyn(Tleaf=Temps,VPD=VPD,Ci=275,
                  Vcmax=Vcmax25,EaV=EaV,EdVC=2e5,delsC=delsV,
                  Jmax = Jmax25,EaJ=EaJ,EdVJ=2e5,delsJ=delsJ)
  
  return(out)
}


# model photosynthesis (PFT specific parameters)

vcmax.1<-t_home_bio[c("Species", "DataSet","Season_New","Temp_Treatment","Tavg_30","Topt_275","Topt_275.se","gMDD0","Rday","Tdiff","PFT","Tdiff_m","MAXSS")]
vcmax.1$PFT[vcmax.1$DataSet=="SAVANNA"] <- "TET_TE" #change PFT


# add PFT specific parameters to the dataframe
vcmax.1<-merge(vcmax.1,pft_param,by="PFT",all=T)



dat_mod_1<-split(vcmax.1,factor(vcmax.1$DataSet))
mod_photo_3<-lapply(dat_mod_1,FUN=model_photo_newf)
topt_mod_3<-get_topts(lapply(mod_photo_3,FUN=topt_from_photosyn))

topt_mod_3$DataSet<-names(dat_mod_1)

topts_cpm_3<-merge(vcmax.1,topt_mod_3,by="DataSet")
topts_cpm_3$PFT[topts_cpm_3$DataSet=="SAVANNA"] <- "BET_TE" #change PFT of SAVANNA back to BET_TE
topts_cpm_3$num <- ave( topts_cpm_3$Topt_275, factor(topts_cpm_3$PFT),FUN = seq_along) # set  unique numbers for colors

# 
# windows(60,60);
# par(cex.lab=1.5,mar=c(5,5,5,5),oma=c(5,5,5,5),cex.axis=1.5,las=1,mfrow=c(1,1))

# pdf(file="Figure2-Photo_vs_Temperature.pdf",width=4,height=4)
# par(mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,0))

#with(t_home.1,plot(MAXSS,Topt,col=COL[factor(DataSet)],pch=16,cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,40),ylim=c(10,35)))

with(topts_cpm_3,plot(Topt_275,topts,bg=COL[PFT],pch=c(21,22,23,24,25)[topts_cpm_3$num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,35),ylim=c(15,35)))
adderrorbars(x=topts_cpm_3$Topt_275,y=topts_cpm_3$topts,SE=topts_cpm_3$Topt_275.se ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(main="dels(Jmax) changed")

#legend("topleft",c("BDT-Te","BET-Te","NET-Br","NET-Te","BET-Tr","Arctic"),col =COL,pch=16,ncol=2,cex=.8,pt.cex=1,pt.lwd=1,title="PFT",bty="n")
abline(a=0,b=1)


title(ylab=expression(Topt[predicted]~(degree*C)),
      xpd=NA,cex.lab=2)
title(xlab=expression(Topt[measured]~(degree*C)),
      xpd=NA,cex.lab=2,adj=.3)


#----------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------
# function to model photosynthesis 
# only JVr changed

model_photo_newf<-function(data){
  
  # get a sequence of leaf temperatures
  Temps <- seq(15,40,by=0.1)
  Tdew=15
  VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)
  
  
  # Vcmax - PFT specific
  Vcmax25<-data$Vcmax25
  
  
  # Activation energy for Jmax, PFT specific
  
  EaJ<-data$EaJ*1000
  
  # deltaS for Vcmax, PFT specific
  delsV<-data$delsV*1000
  #delsV<-with(data,(661.24-(1.24*gMDD0)))
  
  
  #dS for Jmax
  #both adaptationa and acclimation included. when Tgrowth > Thome, dS decrease and vise versa
  
  delsJ<-data$delsJ*1000
  
  # Jmax:Vcmax at 25C
  #both adaptationa and acclimation included. when Tgrowth > Thome, JVr decrease and vise versa
  jvr<-with(data,2.4-(0.021607*MAXSS)+(-0.02762*Tdiff_m)) # adaptation from commongarden experiments
  
  # get Jmax as a function of Vcmax
  Jmax25<-data$Vcmax25*jvr
  
  # Activation energy for Vcmax
  # accimated for growth temperature
  
  #EaV<-with(data,EaV+(1.14*Tdiff))*1000
  
  #EaV<-with(data,42.6+(1.14*Tavg_30))*1000  #acclimation only
  EaV<-data$EaV*1000
  # model photosynthesis at a common Ci=275 ppm
  out <- Photosyn(Tleaf=Temps,VPD=VPD,Ci=275,
                  Vcmax=Vcmax25,EaV=EaV,EdVC=2e5,delsC=delsV,
                  Jmax = Jmax25,EaJ=EaJ,EdVJ=2e5,delsJ=delsJ)
  
  return(out)
}


# model photosynthesis

vcmax.1<-t_home_bio[c("Species", "DataSet","Season_New","Temp_Treatment","Tavg_30","Topt_275","Topt_275.se","gMDD0","Rday","Tdiff","PFT","Tdiff_m","MAXSS")]
vcmax.1$PFT[vcmax.1$DataSet=="SAVANNA"] <- "TET_TE" #change PFT


# add PFT specific parameters to the dataframe
vcmax.1<-merge(vcmax.1,pft_param,by="PFT",all=T)
vcmax.1$DataSet<-factor(vcmax.1$DataSet)
vcmax.1$Species<-factor(vcmax.1$Species)



dat_mod_1<-split(vcmax.1,factor(vcmax.1$DataSet))


mod_photo_b<-lapply(dat_mod_1,FUN=model_photo_newf)
topt_mod_b<-get_topts(lapply(mod_photo_b,FUN=topt_from_photosyn))

topt_mod_b$DataSet<-names(dat_mod_1)

topts_cpm_b<-merge(vcmax.1,topt_mod_b,by="DataSet")
topts_cpm_b$PFT[topts_cpm_b$DataSet=="SAVANNA"] <- "BET_TE" #change PFT of SAVANNA back to BET_TE
topts_cpm_b$num <- ave( topts_cpm_b$Topt_275, factor(topts_cpm_b$PFT),FUN = seq_along) # set  unique numbers for colors


with(topts_cpm_b,plot(Topt_275,topts,bg=COL[PFT],pch=c(21,22,23,24,25)[topts_cpm_3$num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,35),ylim=c(15,35)))
adderrorbars(x=topts_cpm_b$Topt_275,y=topts_cpm_b$topts,SE=topts_cpm_b$Topt_275.se ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(main="JVr changed")

#legend("topleft",c("BDT-Te","BET-Te","NET-Br","NET-Te","BET-Tr","Arctic"),col =COL,pch=16,ncol=2,cex=.8,pt.cex=1,pt.lwd=1,title="PFT",bty="n")
abline(a=0,b=1)


title(ylab=expression(Topt[predicted]~(degree*C)),
      xpd=NA,cex.lab=2)
title(xlab=expression(Topt[measured]~(degree*C)),
      xpd=NA,cex.lab=2,adj=.3)

#-----------------------------------------------------------------

# only EaV changed

model_photo_newf<-function(data){
  
  # get a sequence of leaf temperatures
  Temps <- seq(15,40,by=0.1)
  Tdew=15
  VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)
  
  
  # Vcmax - PFT specific
  Vcmax25<-data$Vcmax25
  
  
  # Activation energy for Jmax, PFT specific
  
  EaJ<-data$EaJ*1000
  
  # deltaS for Vcmax, PFT specific
  delsV<-data$delsV*1000
  #delsV<-with(data,(661.24-(1.24*gMDD0)))
  
  
  #dS for Jmax
  #both adaptationa and acclimation included. when Tgrowth > Thome, dS decrease and vise versa
  
  delsJ<-data$delsJ*1000
  
  # Jmax:Vcmax at 25C
  #both adaptationa and acclimation included. when Tgrowth > Thome, JVr decrease and vise versa
  jvr<-with(data,2.4-(0.021607*MAXSS)+(-0.02762*Tdiff_m)) # adaptation from commongarden experiments
  
  # get Jmax as a function of Vcmax
  Jmax25<-data$Jmax25
  
  # Activation energy for Vcmax
  # accimated for growth temperature
  
  #EaV<-with(data,EaV+(1.14*Tdiff))*1000
  
  EaV<-with(data,42.6+(1.14*Tavg_30))*1000  #acclimation only

  # model photosynthesis at a common Ci=275 ppm
  out <- Photosyn(Tleaf=Temps,VPD=VPD,Ci=275,
                  Vcmax=Vcmax25,EaV=EaV,EdVC=2e5,delsC=delsV,
                  Jmax = Jmax25,EaJ=EaJ,EdVJ=2e5,delsJ=delsJ)
  
  return(out)
}


# model photosynthesis

vcmax.1<-t_home_bio[c("Species", "DataSet","Season_New","Temp_Treatment","Tavg_30","Topt_275","Topt_275.se","gMDD0","Rday","Tdiff","PFT","Tdiff_m","MAXSS")]
vcmax.1$PFT[vcmax.1$DataSet=="SAVANNA"] <- "TET_TE" #change PFT


# add PFT specific parameters to the dataframe
vcmax.1<-merge(vcmax.1,pft_param,by="PFT",all=T)
vcmax.1$DataSet<-factor(vcmax.1$DataSet)
vcmax.1$Species<-factor(vcmax.1$Species)



dat_mod_1<-split(vcmax.1,factor(vcmax.1$DataSet))


mod_photo_c<-lapply(dat_mod_1,FUN=model_photo_newf)
topt_mod_c<-get_topts(lapply(mod_photo_c,FUN=topt_from_photosyn))

topt_mod_c$DataSet<-names(dat_mod_1)

topts_cpm_c<-merge(vcmax.1,topt_mod_c,by="DataSet")
topts_cpm_c$PFT[topts_cpm_c$DataSet=="SAVANNA"] <- "BET_TE" #change PFT of SAVANNA back to BET_TE
topts_cpm_c$num <- ave( topts_cpm_c$Topt_275, factor(topts_cpm_c$PFT),FUN = seq_along) # set  unique numbers for colors


with(topts_cpm_c,plot(Topt_275,topts,bg=COL[PFT],pch=c(21,22,23,24,25)[topts_cpm_3$num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,35),ylim=c(15,35)))
adderrorbars(x=topts_cpm_c$Topt_275,y=topts_cpm_c$topts,SE=topts_cpm_c$Topt_275.se ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(main="EaV changed")

#legend("topleft",c("BDT-Te","BET-Te","NET-Br","NET-Te","BET-Tr","Arctic"),col =COL,pch=16,ncol=2,cex=.8,pt.cex=1,pt.lwd=1,title="PFT",bty="n")
abline(a=0,b=1)


title(ylab=expression(Topt[predicted]~(degree*C)),
      xpd=NA,cex.lab=2)
title(xlab=expression(Topt[measured]~(degree*C)),
      xpd=NA,cex.lab=2,adj=.3)

summary(lm(topts~Topt_275,data=subset(topts_cpm_c,!topts_cpm_c$DataSet %in% c("Hikosaka","WANG_ET_AL","HAN_2"))))
ablineclip(a=3.02282,b=0.89628,x1=16,x2=34,y1=17,y2=34,lty=2,col="red",lwd=3)



#---------------------------------------------------------------------------------------
# EaV and delsJ changed


model_photo_newf<-function(data){
  
  # get a sequence of leaf temperatures
  Temps <- seq(15,40,by=0.1)
  Tdew=15
  VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)
  
  
  # Vcmax - PFT specific
  Vcmax25<-data$Vcmax25
  
  
  # Activation energy for Jmax, PFT specific
  
  EaJ<-data$EaJ*1000
  
  # deltaS for Vcmax, PFT specific
  delsV<-data$delsV*1000
  #delsV<-with(data,(661.24-(1.24*gMDD0)))
  
  
  #dS for Jmax
  #both adaptationa and acclimation included. when Tgrowth > Thome, dS decrease and vise versa
  
  delsJ<-with(data,662.7-(0.97*MAXSS)+(-.562*Tdiff_m))
  
  # Jmax:Vcmax at 25C
  #both adaptationa and acclimation included. when Tgrowth > Thome, JVr decrease and vise versa
  jvr<-with(data,2.4-(0.021607*MAXSS)+(-0.02762*Tdiff_m)) # adaptation from commongarden experiments
  
  # get Jmax as a function of Vcmax
  Jmax25<-data$Jmax25
  
  # Activation energy for Vcmax
  # accimated for growth temperature
  
  #EaV<-with(data,EaV+(1.14*Tdiff))*1000
  
  EaV<-with(data,42.6+(1.14*Tavg_30))*1000  #acclimation only
  
  # model photosynthesis at a common Ci=275 ppm
  out <- Photosyn(Tleaf=Temps,VPD=VPD,Ci=275,
                  Vcmax=Vcmax25,EaV=EaV,EdVC=2e5,delsC=delsV,
                  Jmax = Jmax25,EaJ=EaJ,EdVJ=2e5,delsJ=delsJ)
  
  return(out)
}


# model photosynthesis

vcmax.1<-t_home_bio[c("Species", "DataSet","Season_New","Temp_Treatment","Tavg_30","Topt_275","Topt_275.se","gMDD0","Rday","Tdiff","PFT","Tdiff_m","MAXSS")]
vcmax.1$PFT[vcmax.1$DataSet=="SAVANNA"] <- "TET_TE" #change PFT


# add PFT specific parameters to the dataframe
vcmax.1<-merge(vcmax.1,pft_param,by="PFT",all=T)
vcmax.1$DataSet<-factor(vcmax.1$DataSet)
vcmax.1$Species<-factor(vcmax.1$Species)



dat_mod_1<-split(vcmax.1,factor(vcmax.1$DataSet))


mod_photo_d<-lapply(dat_mod_1,FUN=model_photo_newf)
topt_mod_d<-get_topts(lapply(mod_photo_d,FUN=topt_from_photosyn))

topt_mod_d$DataSet<-names(dat_mod_1)

topts_cpm_d<-merge(vcmax.1,topt_mod_d,by="DataSet")
topts_cpm_d$PFT[topts_cpm_d$DataSet=="SAVANNA"] <- "BET_TE" #change PFT of SAVANNA back to BET_TE
topts_cpm_d$num <- ave( topts_cpm_d$Topt_275, factor(topts_cpm_d$PFT),FUN = seq_along) # set  unique numbers for colors


with(topts_cpm_d,plot(Topt_275,topts,bg=COL[PFT],pch=c(21,22,23,24,25)[topts_cpm_3$num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,35),ylim=c(15,35)))
adderrorbars(x=topts_cpm_d$Topt_275,y=topts_cpm_d$topts,SE=topts_cpm_d$Topt_275.se ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(main="EaV and delsJ changed")

#legend("topleft",c("BDT-Te","BET-Te","NET-Br","NET-Te","BET-Tr","Arctic"),col =COL,pch=16,ncol=2,cex=.8,pt.cex=1,pt.lwd=1,title="PFT",bty="n")
abline(a=0,b=1)


title(ylab=expression(Topt[predicted]~(degree*C)),
      xpd=NA,cex.lab=2)
title(xlab=expression(Topt[measured]~(degree*C)),
      xpd=NA,cex.lab=2,adj=.3)

summary(lm(topts~Topt_275,data=subset(topts_cpm_d,!topts_cpm_d$DataSet %in% c("Hikosaka","WANG_ET_AL","HAN_2"))))
ablineclip(a=3.02282,b=0.89628,x1=16,x2=34,y1=17,y2=34,lty=2,col="red",lwd=3)

#---------------------------------------------------------------------------------------
# EaV and delsJ and JVr changed


model_photo_newf<-function(data){
  
  # get a sequence of leaf temperatures
  Temps <- seq(15,40,by=0.1)
  Tdew=15
  VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)
  
  
  # Vcmax - PFT specific
  Vcmax25<-data$Vcmax25
  
  
  # Activation energy for Jmax, PFT specific
  
  EaJ<-data$EaJ*1000
  
  # deltaS for Vcmax, PFT specific
  delsV<-data$delsV*1000
  #delsV<-with(data,(661.24-(1.24*gMDD0)))
  
  
  #dS for Jmax
  #both adaptationa and acclimation included. when Tgrowth > Thome, dS decrease and vise versa
  
  delsJ<-with(data,662.7-(0.97*MAXSS)+(-.562*Tdiff_m))
  
  # Jmax:Vcmax at 25C
  #both adaptationa and acclimation included. when Tgrowth > Thome, JVr decrease and vise versa
  jvr<-with(data,2.4-(0.021607*MAXSS)+(-0.02762*Tdiff_m)) # adaptation from commongarden experiments
  
  # get Jmax as a function of Vcmax
  Jmax25<-data$Vcmax25*jvr
  
  # Activation energy for Vcmax
  # accimated for growth temperature
  
  #EaV<-with(data,EaV+(1.14*Tdiff))*1000
  
  EaV<-with(data,42.6+(1.14*Tavg_30))*1000  #acclimation only
  
  # model photosynthesis at a common Ci=275 ppm
  out <- Photosyn(Tleaf=Temps,VPD=VPD,Ci=275,
                  Vcmax=Vcmax25,EaV=EaV,EdVC=2e5,delsC=delsV,
                  Jmax = Jmax25,EaJ=EaJ,EdVJ=2e5,delsJ=delsJ)
  
  return(out)
}


# model photosynthesis

vcmax.1<-t_home_bio[c("Species", "DataSet","Season_New","Temp_Treatment","Tavg_30","Topt_275","Topt_275.se","gMDD0","Rday","Tdiff","PFT","Tdiff_m","MAXSS")]
vcmax.1$PFT[vcmax.1$DataSet=="SAVANNA"] <- "TET_TE" #change PFT


# add PFT specific parameters to the dataframe
vcmax.1<-merge(vcmax.1,pft_param,by="PFT",all=T)
vcmax.1$DataSet<-factor(vcmax.1$DataSet)
vcmax.1$Species<-factor(vcmax.1$Species)



dat_mod_1<-split(vcmax.1,factor(vcmax.1$DataSet))


mod_photo_e<-lapply(dat_mod_1,FUN=model_photo_newf)
topt_mod_e<-get_topts(lapply(mod_photo_e,FUN=topt_from_photosyn))

topt_mod_e$DataSet<-names(dat_mod_1)

topts_cpm_e<-merge(vcmax.1,topt_mod_e,by="DataSet")
topts_cpm_e$PFT[topts_cpm_e$DataSet=="SAVANNA"] <- "BET_TE" #change PFT of SAVANNA back to BET_TE
topts_cpm_e$num <- ave( topts_cpm_e$Topt_275, factor(topts_cpm_e$PFT),FUN = seq_along) # set  unique numbers for colors


with(topts_cpm_e,plot(Topt_275,topts,bg=COL[PFT],pch=c(21,22,23,24,25)[topts_cpm_e$num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,35),ylim=c(15,35)))
adderrorbars(x=topts_cpm_e$Topt_275,y=topts_cpm_e$topts,SE=topts_cpm_e$Topt_275.se ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(main="EaV JVr and delsJ changed")

#legend("topleft",c("BDT-Te","BET-Te","NET-Br","NET-Te","BET-Tr","Arctic"),col =COL,pch=16,ncol=2,cex=.8,pt.cex=1,pt.lwd=1,title="PFT",bty="n")
abline(a=0,b=1)


title(ylab=expression(Topt[predicted]~(degree*C)),
      xpd=NA,cex.lab=2)
title(xlab=expression(Topt[measured]~(degree*C)),
      xpd=NA,cex.lab=2,adj=.3)

summary(lm(topts~Topt_275,data=subset(topts_cpm_d,!topts_cpm_d$DataSet %in% c("Hikosaka","WANG_ET_AL","HAN_2"))))
ablineclip(a=3.02282,b=0.89628,x1=16,x2=34,y1=17,y2=34,lty=2,col="red",lwd=3)


#---------------------------------------------------------------------------------------
# EaV and delsJ and JVr changed

data<-subset(vcmax.1,vcmax.1$DataSet=="Hikosaka")
model_photo_newf<-function(data){
  
  # get a sequence of leaf temperatures
  Temps <- seq(15,40,by=0.1)
  Tdew=15
  VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)
  
  
  # Vcmax - PFT specific
  Vcmax25<-65.31
  
  
  # Activation energy for Jmax, PFT specific
  
  EaJ<-29.68*1000
  
  # deltaS for Vcmax, PFT specific
  delsV<-0.629*1000
  #delsV<-with(data,(661.24-(1.24*gMDD0)))
  
  
  #dS for Jmax
  #both adaptationa and acclimation included. when Tgrowth > Thome, dS decrease and vise versa
  
  delsJ<-0.631
  
  # Jmax:Vcmax at 25C
  #both adaptationa and acclimation included. when Tgrowth > Thome, JVr decrease and vise versa
  jvr<-1.76
  
  # get Jmax as a function of Vcmax
  Jmax25<-115.17
  
  # Activation energy for Vcmax
  # accimated for growth temperature
  
  #EaV<-with(data,EaV+(1.14*Tdiff))*1000
  
  EaV<-with(data,42.6+(1.14*Tavg_30))*1000  #acclimation only
  
  # model photosynthesis at a common Ci=275 ppm
  out <- Photosyn(Tleaf=Temps,VPD=VPD,Ci=275,
                  Vcmax=Vcmax25,EaV=EaV,EdVC=2e5,delsC=delsV,
                  Jmax = Jmax25,EaJ=EaJ,EdVJ=2e5,delsJ=delsJ)
  
  return(out)
}


hik_mod<-model_photo_newf(data)
with(hik_mod,plot(Tleaf,(Aj-Rd)))
with(hik_mod,points(Tleaf,(Ac-Rd),col="green"))
with(hik_mod,points(Tleaf,ALEAF,col="red"))




# model photosynthesis

vcmax.1<-t_home_bio[c("Species", "DataSet","Season_New","Temp_Treatment","Tavg_30","Topt_275","Topt_275.se","gMDD0","Rday","Tdiff","PFT","Tdiff_m","MAXSS")]
vcmax.1$PFT[vcmax.1$DataSet=="SAVANNA"] <- "TET_TE" #change PFT


# add PFT specific parameters to the dataframe
vcmax.1<-merge(vcmax.1,pft_param,by="PFT",all=T)
vcmax.1$DataSet<-factor(vcmax.1$DataSet)
vcmax.1$Species<-factor(vcmax.1$Species)



dat_mod_1<-split(vcmax.1,factor(vcmax.1$DataSet))


mod_photo_e<-lapply(dat_mod_1,FUN=model_photo_newf)
topt_mod_e<-get_topts(lapply(mod_photo_e,FUN=topt_from_photosyn))

topt_mod_e$DataSet<-names(dat_mod_1)

topts_cpm_e<-merge(vcmax.1,topt_mod_e,by="DataSet")
topts_cpm_e$PFT[topts_cpm_e$DataSet=="SAVANNA"] <- "BET_TE" #change PFT of SAVANNA back to BET_TE
topts_cpm_e$num <- ave( topts_cpm_e$Topt_275, factor(topts_cpm_e$PFT),FUN = seq_along) # set  unique numbers for colors


with(topts_cpm_e,plot(Topt_275,topts,bg=COL[PFT],pch=c(21,22,23,24,25)[topts_cpm_e$num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,35),ylim=c(15,35)))
adderrorbars(x=topts_cpm_e$Topt_275,y=topts_cpm_e$topts,SE=topts_cpm_e$Topt_275.se ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(main="EaV JVr and delsJ changed")

#legend("topleft",c("BDT-Te","BET-Te","NET-Br","NET-Te","BET-Tr","Arctic"),col =COL,pch=16,ncol=2,cex=.8,pt.cex=1,pt.lwd=1,title="PFT",bty="n")
abline(a=0,b=1)


title(ylab=expression(Topt[predicted]~(degree*C)),
      xpd=NA,cex.lab=2)
title(xlab=expression(Topt[measured]~(degree*C)),
      xpd=NA,cex.lab=2,adj=.3)

summary(lm(topts~Topt_275,data=subset(topts_cpm_d,!topts_cpm_d$DataSet %in% c("Hikosaka","WANG_ET_AL","HAN_2"))))
ablineclip(a=3.02282,b=0.89628,x1=16,x2=34,y1=17,y2=34,lty=2,col="red",lwd=3)





#####################################################################################


data<-subset(vcmax.1,vcmax.1$DataSet=="Hikosaka")
model_photo_newf<-function(data){
  
  # get a sequence of leaf temperatures
  Temps <- seq(15,40,by=0.1)
  Tdew=15
  VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)
  
  
  # Vcmax - PFT specific
  Vcmax25<-data$Vcmax25
  
  
  # Activation energy for Jmax, PFT specific
  
  EaJ<-data$EaJ*1000
  
  # deltaS for Vcmax, PFT specific
  delsV<-data$delsV*1000
  #delsV<-with(data,(661.24-(1.24*gMDD0)))
  
  
  #dS for Jmax
  #both adaptationa and acclimation included. when Tgrowth > Thome, dS decrease and vise versa
  
  delsJ<-with(data,662.7-(0.97*MAXSS)+(-.562*Tdiff_m))
  
  # Jmax:Vcmax at 25C
  #both adaptationa and acclimation included. when Tgrowth > Thome, JVr decrease and vise versa
  jvr<-1.76
  
  # get Jmax as a function of Vcmax
  Jmax25<-data$Jmax25
  
  # Activation energy for Vcmax
  # accimated for growth temperature
  
  #EaV<-with(data,EaV+(1.14*Tdiff))*1000
  
  #EaV<-with(data,42.6+(1.14*Tavg_30))*1000  #acclimation only
  EaV<-59.25714*1000
  # model photosynthesis at a common Ci=275 ppm
  out <- Photosyn(Tleaf=Temps,VPD=VPD,Ci=275,
                  Vcmax=Vcmax25,EaV=EaV,EdVC=2e5,delsC=delsV,
                  Jmax = Jmax25,EaJ=EaJ,EdVJ=2e5,delsJ=delsJ)
  
  return(out)
}


hik_mod<-model_photo_newf(data)
with(hik_mod,plot(Tleaf,(Aj-Rd)))
with(hik_mod,points(Tleaf,(Ac-Rd),col="green"))
with(hik_mod,points(Tleaf,ALEAF,col="red"))

topt_from_photosyn(hik_mod)
